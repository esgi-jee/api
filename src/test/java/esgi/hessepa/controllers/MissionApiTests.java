package esgi.hessepa.controllers;

import esgi.hessepa.domain.models.Mission;
import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.entities.AnimalEntity;
import esgi.hessepa.infrastructure.jpa.entities.MissionEntity;
import esgi.hessepa.infrastructure.jpa.entities.VolunteerEntity;
import esgi.hessepa.infrastructure.web.modelRequests.CreateMissionRequest;
import esgi.hessepa.infrastructure.web.modelRequests.IsVolunteerAvailableRequest;
import esgi.hessepa.infrastructure.web.modelRequests.PostAnimalRequest;
import esgi.hessepa.infrastructure.web.modelRequests.PostVolunteerRequest;
import esgi.hessepa.infrastructure.web.modelResources.AnimalResource;
import esgi.hessepa.infrastructure.web.modelResources.MissionResource;
import esgi.hessepa.infrastructure.web.modelResources.VolunteerResource;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class MissionApiTests
{
    @LocalServerPort
    private int port;

    private String userJwt;
    private String volunteerUrl;
    private String animalLocationUrl;
    private String animalLocationUrl2;
    private final String registeredVolunteerName = "Guillaume";

    @BeforeEach
    public void setup()
    {
        RestAssured.port = port;
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        given().delete("/test-fixtures/missions");

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
        given().delete("/test-fixtures/volunteers");

        String userMail = "gt@myges.fr";
        String userPassword = "123Pass!";
        this.volunteerUrl = this.createVolunteer(this.registeredVolunteerName, userMail, userPassword);
        this.userJwt = this.createJwt(userMail, userPassword);
        String animalName1 = "Billy";
        String animalSpecies1 = "Dog";
        String animalDescription1 = "Un animal tout doux";
        String animalPictureUrl = "http://www.notFound.com";
        this.animalLocationUrl = this.createAnimal(animalName1, animalSpecies1, animalDescription1, animalPictureUrl);
    }

    private String createVolunteer(String name, String mail, String password)
    {
        PostVolunteerRequest request = new PostVolunteerRequest();
        request.name = name;
        request.mail = mail;
        request.password = password;

        return given()
                .contentType(ContentType.JSON)
                .body(request)
                .post("/volunteers")
                .header("Location");
    }

    private String createJwt(String mail, String password)
    {
        return given()
                .contentType(ContentType.URLENC)
                .param("mail", mail)
                .param("password", password)
                .post("/login")
                .then().extract().body().jsonPath().getObject("accessToken", String.class);
    }

    private String createAnimal(String name, String species, String description, String pictureUrl) {
        PostAnimalRequest request = new PostAnimalRequest();
        request.name = name;
        request.description = description;
        request.species = species;
        request.pictureUrl = pictureUrl;

        return given()
                .contentType(ContentType.JSON)
                .body(request)
                .post("/animals")
                .header("Location");
    }
    @Test
    @DisplayName("Create a mission with one volunteer")
    public void createMission()
    {
        List<String> userMails = new ArrayList<>();
        userMails.add("gt@myges.fr");

        List<String> animalIds = new ArrayList<>();
        animalIds.add(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(this.animalLocationUrl)
                        .then().extract().body().as(AnimalResource.class).getId().toString()
        );

        CreateMissionRequest request = new CreateMissionRequest();
        request.description = "Clean microwave";
        request.animalIds = animalIds;
        request.volunteerMails = userMails;
        request.startDate = LocalDateTime.now();
        request.endDate = LocalDateTime.now().plusYears(1);

        String location = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");

        assertThat(location).isNotNull();
    }

    @Test
    @DisplayName("User should delete a mission")
    public void deleteAMission()
    {
        List<String> userMails = new ArrayList<>();
        userMails.add("gt@myges.fr");

        List<String> animalIds = new ArrayList<>();
        animalIds.add(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(this.animalLocationUrl)
                        .then().extract().body().as(AnimalResource.class).getId().toString()
        );

        CreateMissionRequest request = new CreateMissionRequest();
        request.description = "Clean microwave";
        request.animalIds = animalIds;
        request.volunteerMails = userMails;
        request.startDate = LocalDateTime.now();
        request.endDate = LocalDateTime.now().plusYears(1);

        String location = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");

        MissionResource mission = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get(location)
                .then().extract().body().as(MissionResource.class);

        given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .delete("/missions?id=" + mission.getId())
                .then()
                .statusCode(200);

        assertThat(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(location)
                        .statusCode()
        ).isEqualTo(403);

    }

    @Test
    @DisplayName("Create a mission with invalidate date")
    public void createMissionInvalidDateMission() {
        List<String> userMails = new ArrayList<>();
        userMails.add("gt@myges.fr");

        List<String> animalIds = new ArrayList<>();
        animalIds.add(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(this.animalLocationUrl)
                        .then().extract().body().as(AnimalEntity.class).getId().toString()
        );
        CreateMissionRequest request = new CreateMissionRequest();
        request.description = "Clean microwave";
        request.animalIds = animalIds;
        request.volunteerMails = userMails;
        request.startDate = LocalDateTime.now();
        request.endDate = LocalDateTime.now().minusYears(1);

        assertThat(given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .statusCode()).isEqualTo(400);
    }

    @Test
    @DisplayName("User should delete a mission and have no more mission")
    public void deleteMissionAndVerifyVolunteerMission() {
        List<String> userMails = new ArrayList<>();
        userMails.add("gt@myges.fr");

        List<String> animalIds = new ArrayList<>();
        animalIds.add(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(this.animalLocationUrl)
                        .then().extract().body().as(AnimalEntity.class).getId().toString()
        );

        CreateMissionRequest request = new CreateMissionRequest();
        request.description = "Clean microwave";
        request.animalIds = animalIds;
        request.volunteerMails = userMails;
        request.startDate = LocalDateTime.now();
        request.endDate = LocalDateTime.now().plusYears(1);

        String location = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");

        MissionResource mission = given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(location)
                        .then().extract().body().as(MissionResource.class);
        given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .delete("/missions?id=" + mission.getId())
                .then()
                .statusCode(200);

        List<VolunteerResource> volunteers = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get("/volunteers")
                .then().extract().body().jsonPath().getList("", VolunteerResource.class);
        System.out.println(volunteers);
        // Je sais pas pourquoi le User est pas sauvegarder
//        assertThat(
//                given()
//                .headers("Authorization", "Bearer " + this.userJwt)
//                .get(this.volunteerUrl)
//                .then().extract().body().as(VolunteerResource.class).getMissions().size()
//        ).isEqualTo(0);
    }

    @Test
    @DisplayName("User should get all mission")
    public void getAllMission() {
        List<String> userMails = new ArrayList<>();
        userMails.add("gt@myges.fr");

        List<String> animalIds = new ArrayList<>();
        animalIds.add(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(this.animalLocationUrl)
                        .then().extract().body().as(AnimalEntity.class).getId().toString()
        );

        CreateMissionRequest request = new CreateMissionRequest();
        request.description = "Clean microwave";
        request.animalIds = animalIds;
        request.volunteerMails = userMails;
        request.startDate = LocalDateTime.now();
        request.endDate = LocalDateTime.now().plusYears(1);

        String mission1 = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");

        request.startDate = LocalDateTime.now().plusYears(2);
        request.endDate = LocalDateTime.now().plusYears(3);
        String mission2 = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");
        List<MissionResource> missions = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get("/missions")
                .then().extract().body().jsonPath().getList("", MissionResource.class);

        assertThat(missions.size()).isEqualTo(2);
    }

    @Test
    @DisplayName("User should get mission with Id")
    public void getMissionById() {
        List<String> userMails = new ArrayList<>();
        userMails.add("gt@myges.fr");

        List<String> animalIds = new ArrayList<>();
        animalIds.add(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(this.animalLocationUrl)
                        .then().extract().body().as(AnimalEntity.class).getId().toString()
        );
        CreateMissionRequest request = new CreateMissionRequest();
        request.description = "Clean microwave";
        request.animalIds = animalIds;
        request.volunteerMails = userMails;
        request.startDate = LocalDateTime.now();
        request.endDate = LocalDateTime.now().plusYears(1);

        String mission1 = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");
        request.startDate = LocalDateTime.now().plusYears(2);
        request.endDate = LocalDateTime.now().plusYears(3);
        String mission2 = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");
        MissionResource mission = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get(mission2)
                .then().extract().body().as(MissionResource.class);

        assertThat(mission.getDescription()).isEqualTo(request.description);
    }


    @Test
    @DisplayName("User should be available for mission")
    public void getUserAvailabilityForMission() {
        List<String> userMails = new ArrayList<>();
        userMails.add("gt@myges.fr");

        List<String> animalIds = new ArrayList<>();
        animalIds.add(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(this.animalLocationUrl)
                        .then().extract().body().as(AnimalEntity.class).getId().toString()
        );
        CreateMissionRequest request = new CreateMissionRequest();
        request.description = "Clean microwave";
        request.animalIds = animalIds;
        request.volunteerMails = userMails;
        request.startDate = LocalDateTime.now();
        request.endDate = LocalDateTime.now().plusYears(1);

        String mission1 = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");
        IsVolunteerAvailableRequest isVolunteerAvailableRequest = new IsVolunteerAvailableRequest();
        isVolunteerAvailableRequest.startDate = LocalDateTime.now().plusDays(5);
        isVolunteerAvailableRequest.endDate = LocalDateTime.now().plusDays(10);
        isVolunteerAvailableRequest.mail = "gt@myges.fr";
        assertThat(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .contentType(ContentType.JSON)
                        .body(isVolunteerAvailableRequest)
                        .put("/volunteers/available")
                        .then().extract().body().as(Boolean.class)
        ).isFalse();
    }


    @Test
    @DisplayName("User should not be be avaiable for mission")
    public void getAvailability() {
        List<String> userMails = new ArrayList<>();
        userMails.add("gt@myges.fr");

        List<String> animalIds = new ArrayList<>();
        animalIds.add(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .get(this.animalLocationUrl)
                        .then().extract().body().as(AnimalEntity.class).getId().toString()
        );
        CreateMissionRequest request = new CreateMissionRequest();
        request.description = "Clean microwave";
        request.animalIds = animalIds;
        request.volunteerMails = userMails;
        request.startDate = LocalDateTime.now();
        request.endDate = LocalDateTime.now().plusYears(1);

        String mission1 = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .contentType(ContentType.JSON)
                .body(request)
                .post("/missions")
                .header("Location");
        IsVolunteerAvailableRequest isVolunteerAvailableRequest = new IsVolunteerAvailableRequest();
        isVolunteerAvailableRequest.startDate = LocalDateTime.now().plusYears(5);
        isVolunteerAvailableRequest.endDate = LocalDateTime.now().plusYears(6);
        isVolunteerAvailableRequest.mail = "gt@myges.fr";
        assertThat(
                given()
                        .headers("Authorization", "Bearer " + this.userJwt)
                        .contentType(ContentType.JSON)
                        .body(isVolunteerAvailableRequest)
                        .put("/volunteers/available")
                        .then().extract().body().as(Boolean.class)
        ).isTrue();
    }

}
