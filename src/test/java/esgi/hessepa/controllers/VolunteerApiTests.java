package esgi.hessepa.controllers;

import esgi.hessepa.infrastructure.jpa.entities.VolunteerEntity;
import esgi.hessepa.infrastructure.web.modelRequests.PostVolunteerRequest;
import esgi.hessepa.infrastructure.web.modelResources.VolunteerResource;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
class VolunteerApiTests
{
    @LocalServerPort
    private int port;

    private String volunteerLocationUrl;
    private String userJwt;
    private String adminJwt;
    private String userToDeleteJwt;

    private final String registeredVolunteerName = "Guillaume";
    private final String registeredAdminName = "Touchet";

    @BeforeEach
    public void setup()
    {
        RestAssured.port = port;
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
        given().delete("/test-fixtures/volunteers");

        String userMail = "gt@myges.fr";
        String userPassword = "123Pass!";
        this.volunteerLocationUrl = this.createVolunteer(this.registeredVolunteerName, userMail, userPassword);
        this.userJwt = this.createJwt(userMail, userPassword);

        String adminMail = "gtouchet@myges.fr";
        String adminPassword = "**P4ssw0rD**";
        this.createAdmin(this.registeredAdminName, adminMail, adminPassword);
        this.adminJwt = this.createJwt(adminMail, adminPassword);
    }

    private String createVolunteer(String name, String mail, String password)
    {
        PostVolunteerRequest request = new PostVolunteerRequest();
        request.name = name;
        request.mail = mail;
        request.password = password;

        return given()
                .contentType(ContentType.JSON)
                .body(request)
                .post("/volunteers")
                .header("Location");
    }

    private void createAdmin(String name, String mail, String password)
    {
        PostVolunteerRequest request = new PostVolunteerRequest();
        request.name = name;
        request.mail = mail;
        request.password = password;

        given()
                .contentType(ContentType.JSON)
                .body(request)
                .post("/volunteers");

        given().put("/test-fixtures/volunteers?mail=" + mail + "&role=ADMIN");
    }

    private String createJwt(String mail, String password)
    {
        return given()
                .contentType(ContentType.URLENC)
                .param("mail", mail)
                .param("password", password)
                .post("/login")
                .then().extract().body().jsonPath().getObject("accessToken", String.class);
    }

    @Test
    @DisplayName("Read all volunteers, should return a list of 2 volunteers")
    public void createVolunteer()
    {
        List<VolunteerEntity> volunteers = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get("/volunteers")
                .then().extract().body().jsonPath().getList("", VolunteerEntity.class);

        assertThat(volunteers.size()).isEqualTo(2);
    }

    @Test
    @DisplayName("Read the volunteer by its location URL, should return the volunteer")
    public void readVolunteerByLocationUrl()
    {
        VolunteerResource volunteer = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get(this.volunteerLocationUrl)
                .then().extract().body().as(VolunteerResource.class);

        assertThat(volunteer.getName()).isEqualTo(this.registeredVolunteerName);
    }

    @Test
    @DisplayName("A user should not be able to delete an account, should return 403")
    public void shouldNotDeleteUser()
    {
        given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .delete("/volunteers?mail=gt@myges.fr")
                .then()
                .statusCode(403);
    }

    @Test
    @DisplayName("An admin should be able to delete an account, should return 200")
    public void shouldDeleteUser()
    {
        given()
                .headers("Authorization", "Bearer " + this.adminJwt)
                .delete("/volunteers?mail=gt@myges.fr")
                .then()
                .statusCode(200);
    }

    @Test
    @DisplayName("Get volunteers by name 'Guilla', should return a list of 1 volunteer, with name 'Guillaume'")
    public void shouldReadVolunteerByName_1()
    {
        VolunteerEntity volunteer = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get("/volunteers/name?name=Guilla")
                .then().extract().body().jsonPath().getList("", VolunteerEntity.class)
                .get(0);

        assertThat(volunteer.getName()).isEqualTo("Guillaume");
    }

    @Test
    @DisplayName("Get volunteers by name 'Guiume', should return a list of 1 volunteer, with name 'Guillaume'")
    public void shouldReadVolunteerByName_2()
    {
        VolunteerEntity volunteer = given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get("/volunteers/name?name=Guiume")
                .then().extract().body().jsonPath().getList("", VolunteerEntity.class)
                .get(0);

        assertThat(volunteer.getName()).isEqualTo("Guillaume");
    }

    @Test
    @DisplayName("Get volunteers by name 'Gui', should not return any volunteer because name similarity is too low, should return 404")
    public void shouldNotReadVolunteerByName()
    {
        given()
                .headers("Authorization", "Bearer " + this.userJwt)
                .get("/volunteers/name?name=Gui")
                .then()
                .statusCode(404);
    }
}
