package esgi.hessepa.services;

import esgi.hessepa.TestsInitializer;
import esgi.hessepa.application.utilitaries.MailSenderGmail;
import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.models.Species;
import esgi.hessepa.infrastructure.jpa.Mapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.anyOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class MailServiceTest extends TestsInitializer {

    @Test
    @DisplayName("Should return the formatted list of aniamls")
    public void shouldReturnFormattedMail()
    {
        MailSenderGmail mailSenderGmail = new MailSenderGmail();

        super.animalRepository.save(Animal.of("billyLeChien", Species.of("DOG"), "il est bleu", "http://images.io/image.png"));
        super.animalRepository.save(Animal.of("billyLeChien2", Species.of("DOG"), "il est vert", "http://images.io/image.png"));

        assertThat(mailSenderGmail.generateMailBody(super.animalRepository.getAll()))
                .isIn("Bonjour, voici les animaux dont il faudrait s'occuper: billyLeChien billyLeChien2 ",
                "Bonjour, voici les animaux dont il faudrait s'occuper: billyLeChien2 billyLeChien ");

    }
}
