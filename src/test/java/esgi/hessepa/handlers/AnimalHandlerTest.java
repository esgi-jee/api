package esgi.hessepa.handlers;

import esgi.hessepa.TestsInitializer;
import esgi.hessepa.application.dtos.animal.SaveAnimalDto;
import esgi.hessepa.application.handlers.animal.GetAllAnimalsHandler;
import esgi.hessepa.application.handlers.animal.GetByIdAnimalHandler;
import esgi.hessepa.application.handlers.animal.SaveAnimalHandler;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.application.handlers.volunteer.GetAllVolunteersHandler;
import esgi.hessepa.application.handlers.volunteer.GetByMailVolunteerHandler;
import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.models.Species;
import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.models.exceptions.UnknownSpeciesException;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.apis.AnimalPicturesApi;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.assertThat;

public class AnimalHandlerTest extends TestsInitializer {


    @Test
    @DisplayName("Save a new Animal")
    public void saveAnimal()
    {
        SaveAnimalHandler saveAnimalHandler = new SaveAnimalHandler(
                super.animalRepository,
                new AnimalPicturesApi()
        );

        saveAnimalHandler.handle(new SaveAnimalDto("billyLeChien", "DOG", "il est bleu", "http://images.io/image.png"));
        assertThat(super.animalRepository.getAll().size()).isEqualTo(1);
    }

    @Test
    @DisplayName("Get all animal, should return a list of 2 animal")
    public void getAllAnimal()
    {

        GetAllAnimalsHandler getAllAnimalsHandler = new GetAllAnimalsHandler(
                super.animalRepository
        );

        super.animalRepository.save(Animal.of("billyLeChien", Species.of("DOG"), "il est bleu", "http://images.io/image.png"));
        super.animalRepository.save(Animal.of("billyLeChien2", Species.of("DOG"), "il est bleu, mais plus que billy", "http://images.io/image.png"));
        assertThat(super.animalRepository.getAll().size()).isEqualTo(2);


        assertThat((getAllAnimalsHandler.handle(null)).size()).isEqualTo(2);
    }

    @Test
    @DisplayName("Get animal by id")
    public void getAnimalById ()
    {

        GetByIdAnimalHandler getByIdAnimalHandler = new GetByIdAnimalHandler(
                super.animalRepository
        );

        EntityId entityId = EntityId.generate();
        super.animalRepository.save(Animal.of(entityId,"billyLeChien", Species.of("DOG"), "il est bleu", "http://images.io/image.png"));
        super.animalRepository.save(Animal.of("billyLeChien2", Species.of("DOG"), "il est bleu, mais plus que billy", "http://images.io/image.png"));
        assertThat(super.animalRepository.getAll().size()).isEqualTo(2);


        assertThat((getByIdAnimalHandler.handle(entityId)).getName()).isEqualTo("billyLeChien");

    }

}
