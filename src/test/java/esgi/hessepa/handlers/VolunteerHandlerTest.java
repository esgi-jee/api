package esgi.hessepa.handlers;

import esgi.hessepa.TestsInitializer;
import esgi.hessepa.application.dtos.volunteer.SaveVolunteerDto;
import esgi.hessepa.application.dtos.volunteer.SubscribeToNewLettersDto;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.application.handlers.volunteer.*;
import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.web.controllers.configurations.VolunteerConfiguration;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class VolunteerHandlerTest extends TestsInitializer
{
    @Test
    @DisplayName("Read all volunteers, should return a list of 1 volunteers")
    public void saveVolunteerHandler()
    {
        SaveVolunteerHandler saveVolunteerHandler = new SaveVolunteerHandler(
                super.volunteerRepository,
                new VolunteerConfiguration(),
                new BCryptPasswordEncoder()
        );

        saveVolunteerHandler.handle(new SaveVolunteerDto("billy", "billy@billy.com", "7584629&tB", null, null, false));
        assertThat(super.volunteerRepository.getAll().size()).isEqualTo(1);
    }

    @Test
    @DisplayName("Create 2 volunteer then delete 1")
    public void deleteVolunteerHandler()
    {
        DeleteVolunteerHandler deleteVolunteerHandler = new DeleteVolunteerHandler(
                super.volunteerRepository
        );

        super.volunteerRepository.save(Volunteer.of("billy@billy.com", "7584629&tB", "billy", null, null, null, false ));
        super.volunteerRepository.save(Volunteer.of("billy2@billy.com", "7584629&tB", "billy2", null, null, null, true ));
        assertThat(super.volunteerRepository.getAll().size()).isEqualTo(2);

        Volunteer userId = super.volunteerRepository.getAll().get(0);
        deleteVolunteerHandler.handle(userId);

        assertThat(super.volunteerRepository.getAll().get(0).getId()).isNotEqualTo(userId.getId());

    }

    @Test
    @DisplayName("Get all volunteer, should return a list of 2 volunteer")
    public void getAllVolunteer()
    {

        GetAllVolunteersHandler getAllVolunteersHandler = new GetAllVolunteersHandler(
                super.volunteerRepository
        );

        super.volunteerRepository.save(Volunteer.of("billy@billy.com", "7584629&tB", "billy", null, null, null, false ));
        super.volunteerRepository.save(Volunteer.of("billy2@billy.com", "7584629&tB", "billy2", null, null, null, true));
        assertThat(super.volunteerRepository.getAll().size()).isEqualTo(2);


        assertThat((getAllVolunteersHandler.handle(null)).size()).isEqualTo(2);
    }

    @Test
    @DisplayName("Get volunteer by mail")
    public void getVolunteerByMail ()
    {

        GetByMailVolunteerHandler getByMailVolunteerHandler = new GetByMailVolunteerHandler(
                super.volunteerRepository
        );

        super.volunteerRepository.save(Volunteer.of("billy@billy.com", "7584629&tB", "billy", null, null, null, true));
        super.volunteerRepository.save(Volunteer.of("billy2@billy.com", "7584629&tB", "billy2", null, null, null, false ));
        assertThat(super.volunteerRepository.getAll().size()).isEqualTo(2);


        assertThat(getByMailVolunteerHandler.handle("billy2@billy.com").getMail()).isEqualTo("billy2@billy.com");
    }

    @Test
    @DisplayName("Get volunteer by name")
    public void getVolunteerByName ()
    {
        GetByNameVolunteersHandler getByNameVolunteersHandler = new GetByNameVolunteersHandler(
                super.volunteerRepository,
                new LevenshteinDistance()
        );

        super.volunteerRepository.save(Volunteer.of("billy@billy.com", "7584629&tB", "billy", null, null, null, true ));
        super.volunteerRepository.save(Volunteer.of("billy2@billy.com", "7584629&tB", "billy2", null, null, null, false ));
        super.volunteerRepository.save(Volunteer.of("bob@bob.com", "7584629&tB", "bob", null, null, null, false ));
        assertThat(super.volunteerRepository.getAll().size()).isEqualTo(3);


        assertThat(getByNameVolunteersHandler.handle("billy").size()).isEqualTo(2);
    }

    @Test
    @DisplayName("Should return error not found")
    public void shouldNotUpdateVolunteerSubscriber ()
    {
        SubscribeToNewslettersHandler subscribeToNewslettersHandler = new SubscribeToNewslettersHandler(
                super.volunteerRepository
        );

        super.volunteerRepository.save(Volunteer.of("billy@billy.com", "7584629&tB", "billy", null, null, null, true ));
        super.volunteerRepository.save(Volunteer.of("billy2@billy.com", "7584629&tB", "billy2", null, null, null, false ));
        super.volunteerRepository.save(Volunteer.of("bob@bob.com", "7584629&tB", "bob", null, null, null, false ));
        assertThat(super.volunteerRepository.getAll().size()).isEqualTo(3);

        try {
            subscribeToNewslettersHandler.handle(new SubscribeToNewLettersDto("a", false));

        }catch (HandlerException e) {
            assertThat(e).isInstanceOf(HandlerException.class);
        }
    }

    @Test
    @DisplayName("Should return updatedVolunteer")
    public void shouldUpdateVolunteerSubscriber ()
    {

        SubscribeToNewslettersHandler subscribeToNewslettersHandler = new SubscribeToNewslettersHandler(
                super.volunteerRepository
        );
        EntityId entityId = EntityId.generate();
        super.volunteerRepository.save(Volunteer.of("billy@billy.com", "7584629&tB", "billy", null, null, null, true ));
        super.volunteerRepository.save(Volunteer.of("billy2@billy.com", "7584629&tB", "billy2", null, null, null, false ));
        super.volunteerRepository.save(Volunteer.of(entityId, "bob@bob.com", "7584629&tB", "bob", null, null, null, null, false ));
        assertThat(super.volunteerRepository.getAll().size()).isEqualTo(3);


        assertThat(subscribeToNewslettersHandler.handle(new SubscribeToNewLettersDto(entityId.toString(), true)).isSubscribedToNewsletters()).isTrue();
    }
}
