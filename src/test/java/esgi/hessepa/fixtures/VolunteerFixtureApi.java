package esgi.hessepa.fixtures;

import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.infrastructure.jpa.jpaRepositories.EntityNotFoundException;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

@Profile("!prod")
@RestController
@RequestMapping("/test-fixtures/volunteers")
public class VolunteerFixtureApi
{
    private final VolunteerRepositoryInterface volunteerRepository;

    public VolunteerFixtureApi(VolunteerRepositoryInterface volunteerRepository)
    {
        this.volunteerRepository = volunteerRepository;
    }

    @DeleteMapping
    public void clearVolunteers()
    {
        this.volunteerRepository.removeAll();
    }

    @PutMapping
    public void updateVolunteerRole(@RequestParam String mail, @RequestParam String role)
    {
        String mailDebug = mail.replace("%40", "@");

        Volunteer volunteer = this.volunteerRepository.getAll().stream()
                .filter(v -> v.getMail().equals(mailDebug))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(null));

        Volunteer admin = Volunteer.of(
                volunteer.getId(),
                volunteer.getMail(),
                volunteer.getPassword(),
                volunteer.getName(),
                role,
                volunteer.getJob(),
                volunteer.getPictureUrl(),
                volunteer.getMissions(),
                volunteer.isSubscribedToNewsletters()
        );

        this.volunteerRepository.save(admin);
    }
}
