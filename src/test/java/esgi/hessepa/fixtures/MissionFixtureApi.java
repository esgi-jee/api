package esgi.hessepa.fixtures;

import esgi.hessepa.kernel.repositories.MissionRepositoryInterface;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Profile("!prod")
@RestController
@RequestMapping("/test-fixtures/missions")
public class MissionFixtureApi
{
    private final MissionRepositoryInterface missionRepository;

    public MissionFixtureApi(MissionRepositoryInterface missionRepository)
    {
        this.missionRepository = missionRepository;
    }

    @DeleteMapping
    public void clearMissions()
    {
        this.missionRepository.removeAll();
    }
}
