package esgi.hessepa;

import esgi.hessepa.domain.inMemoryRepositories.AnimalInMemoryRepository;
import esgi.hessepa.domain.inMemoryRepositories.VolunteerInMemoryRepository;
import esgi.hessepa.kernel.repositories.AnimalRepositoryInterface;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.rules.ExpectedException;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.Resource;

@ActiveProfiles("test")
public abstract class TestsInitializer
{
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Resource
    protected VolunteerRepositoryInterface volunteerRepository;
    @Resource
    protected AnimalRepositoryInterface animalRepository;

    @BeforeEach
    public void setup()
    {
        this.volunteerRepository = new VolunteerInMemoryRepository();
        this.animalRepository = new AnimalInMemoryRepository();
    }
}

