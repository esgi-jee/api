package esgi.hessepa.domain;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

public class ArchitectureTest
{
    JavaClasses projectClasses;

    @BeforeEach
    void setup()
    {
        projectClasses = new ClassFileImporter().importPackages("esgi.hessepa");
    }

    @Disabled("esgi.hessepa.domain.models.Role implements a Spring interface, so test is failing")
    @Test
    void should_domain_never_be_linked_with_frameworks()
    {
        //
        var ruleNoFramework = noClasses().that().resideInAPackage("..domain..")
                .should().dependOnClassesThat().resideInAPackage("..springframework..")
                .orShould().dependOnClassesThat().resideInAPackage("javax..");

        ruleNoFramework.check(projectClasses);
    }

    @Disabled("TODO: how ??")
    @Test
    void should_respect_hexagonal_architecture()
    {
        var ruleLayerAccess = layeredArchitecture()
                .layer("domain").definedBy("..domain..")
                .layer("infrastructure").definedBy("..infrastructure..")
                .whereLayer("domain").mayOnlyBeAccessedByLayers("infrastructure");

        ruleLayerAccess.check(projectClasses);
    }
}
