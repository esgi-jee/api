package esgi.hessepa.infrastructure.apis;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

@Service
public class AnimalPicturesApi
{
    public String getCatPictureUrl() throws IOException
    {
        return this.get("https://api.thecatapi.com/v1/images/search");
    }

    public String getDogPictureUrl() throws IOException
    {
        return this.get("https://random.dog/woof.json?ref=apilist.fun");
    }

    private String get(String url) throws IOException
    {
        URL apiUrl = new URL(url);
        HttpURLConnection conn = (HttpURLConnection)apiUrl.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();

        if (conn.getResponseCode() == 200)
        {
            Scanner scan = new Scanner(apiUrl.openStream());
            StringBuilder json = new StringBuilder();
            while(scan.hasNext())
            {
                json.append(scan.nextLine());
            }
            scan.close();

            return JsonParser.parseString(json.toString()).isJsonArray() ?
                    new Gson().fromJson(json.toString(), JsonObject[].class)[0].get("url").getAsString() :
                    new Gson().fromJson(json.toString(), JsonObject.class).get("url").getAsString();
        }
        return null;
    }
}
