package esgi.hessepa.infrastructure.spring;

import esgi.hessepa.application.handlers.animal.GetAllAnimalsHandler;
import esgi.hessepa.application.handlers.animal.GetByIdAnimalHandler;
import esgi.hessepa.application.handlers.animal.SaveAnimalHandler;
import esgi.hessepa.application.handlers.mission.CompleteMissionHandler;
import esgi.hessepa.application.handlers.mission.GetAllMissionsHandler;
import esgi.hessepa.application.handlers.mission.GetByIdMissionHandler;
import esgi.hessepa.application.handlers.mission.SaveMissionHandler;
import esgi.hessepa.application.handlers.volunteer.DeleteVolunteerHandler;
import esgi.hessepa.application.handlers.volunteer.GetAllVolunteersHandler;
import esgi.hessepa.application.handlers.volunteer.GetByMailVolunteerHandler;
import esgi.hessepa.application.handlers.volunteer.SaveVolunteerHandler;
import esgi.hessepa.application.utilitaries.MailSenderGmail;
import esgi.hessepa.kernel.repositories.AnimalRepositoryInterface;
import esgi.hessepa.kernel.repositories.MissionRepositoryInterface;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.application.handlers.volunteer.*;
import esgi.hessepa.infrastructure.web.controllers.configurations.VolunteerConfiguration;
import esgi.hessepa.infrastructure.apis.AnimalPicturesApi;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public final class SpringHandlers {
    private final VolunteerRepositoryInterface volunteerRepository;

    private final MissionRepositoryInterface missionRepository;
    private final VolunteerConfiguration volunteerConfiguration;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final LevenshteinDistance levenshteinDistance;
    private final MailSenderGmail mailSenderGmail;

    private final AnimalRepositoryInterface animalRepository;
    private final AnimalPicturesApi animalPicturesApi;

    public SpringHandlers(
            VolunteerRepositoryInterface volunteerRepository,
            MissionRepositoryInterface missionRepository,
            VolunteerConfiguration volunteerConfiguration,
            BCryptPasswordEncoder bCryptPasswordEncoder,
            LevenshteinDistance levenshteinDistance,
            MailSenderGmail mailSenderGmail,
            AnimalRepositoryInterface animalRepository,
            AnimalPicturesApi animalPicturesApi
    ) {
        this.volunteerRepository = volunteerRepository;
        this.missionRepository = missionRepository;
        this.volunteerConfiguration = volunteerConfiguration;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.levenshteinDistance = levenshteinDistance;
        this.mailSenderGmail = mailSenderGmail;
        this.animalRepository = animalRepository;
        this.animalPicturesApi = animalPicturesApi;
    }

    /**
     * Volunteer handlers
     */
    @Bean
    public SaveVolunteerHandler saveVolunteer() {
        return new SaveVolunteerHandler(volunteerRepository, volunteerConfiguration, bCryptPasswordEncoder);
    }

    @Bean
    public GetAllVolunteersHandler getAllVolunteers() {
        return new GetAllVolunteersHandler(volunteerRepository);
    }

    @Bean
    public GetByMailVolunteerHandler getByMailVolunteer() {
        return new GetByMailVolunteerHandler(volunteerRepository);
    }

    @Bean
    public DeleteVolunteerHandler deleteVolunteer() {
        return new DeleteVolunteerHandler(volunteerRepository);
    }

    @Bean
    public GetByNameVolunteersHandler getByNameVolunteers() {
        return new GetByNameVolunteersHandler(volunteerRepository, levenshteinDistance);
    }

    @Bean
    public SendMailToSubscribedVolunteerHandler sendMailToSubscribedVolunteer() {
        return new SendMailToSubscribedVolunteerHandler(volunteerRepository, mailSenderGmail);
    }

    @Bean
    public SubscribeToNewslettersHandler subscribeToNewsletters() {
        return new SubscribeToNewslettersHandler(volunteerRepository);
    }

    @Bean
    public IsVolunteerAvailableHandler isVolunteerAvailable() {
        return new IsVolunteerAvailableHandler(this.volunteerRepository);
    }

    /**
     * Animal handlers
     */
    @Bean
    public GetAllAnimalsHandler getAllAnimals() {
        return new GetAllAnimalsHandler(animalRepository);
    }

    @Bean
    public GetByIdAnimalHandler getByIdAnimal() {
        return new GetByIdAnimalHandler(animalRepository);
    }

    @Bean
    public SaveAnimalHandler saveAnimal() {
        return new SaveAnimalHandler(animalRepository, animalPicturesApi);
    }


    //Mission handlers
    @Bean
    public SaveMissionHandler saveMission() {
        return new SaveMissionHandler(missionRepository, volunteerRepository, animalRepository);
    }
    @Bean
    public GetAllMissionsHandler getAllMissions() {
        return new GetAllMissionsHandler(missionRepository);
    }
    @Bean
    public GetByIdMissionHandler getByIdMission() {
        return new GetByIdMissionHandler(missionRepository);
    }
    @Bean
    public CompleteMissionHandler completeMission() {
        return new CompleteMissionHandler(this.missionRepository);
    }
}
