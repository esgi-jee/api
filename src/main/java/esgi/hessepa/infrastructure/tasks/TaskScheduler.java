package esgi.hessepa.infrastructure.tasks;

import esgi.hessepa.application.handlers.animal.GetAllAnimalsHandler;
import esgi.hessepa.application.handlers.volunteer.SendMailToSubscribedVolunteerHandler;
import org.springframework.stereotype.Service;

import java.util.Timer;

@Service
public class TaskScheduler {
    public static final long ONE_DAY = 1000 * 60 * 60 * 24;
    public static final long ONE_MINUTE = 1000 * 60;


    private SendMailToSubscribedVolunteerHandler sendMailToSubscribedVolunteerHandler;
    private GetAllAnimalsHandler getAllAnimalsHandler;

    public TaskScheduler(
            SendMailToSubscribedVolunteerHandler sendMailToSubscribedVolunteerHandler,
            GetAllAnimalsHandler getAllAnimalsHandler
    ) {
        this.getAllAnimalsHandler = getAllAnimalsHandler;
        this.sendMailToSubscribedVolunteerHandler = sendMailToSubscribedVolunteerHandler;
    }

    public void start() {
        Timer timer = new Timer();
        //scheduler will launch NewsletterTask every 24h (or any period you want)
        timer.schedule(new NewsletterTask(
                this.sendMailToSubscribedVolunteerHandler,
                this.getAllAnimalsHandler
        ), 1000, ONE_MINUTE);
    }
}
