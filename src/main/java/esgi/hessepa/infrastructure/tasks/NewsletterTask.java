package esgi.hessepa.infrastructure.tasks;

import esgi.hessepa.application.handlers.animal.GetAllAnimalsHandler;
import esgi.hessepa.application.handlers.volunteer.SendMailToSubscribedVolunteerHandler;

import java.util.Date;
import java.util.TimerTask;

public final class NewsletterTask extends TimerTask {


    private SendMailToSubscribedVolunteerHandler sendMailToSubscribedVolunteerHandler;
    private GetAllAnimalsHandler getAllAnimalsHandler;

    public NewsletterTask(
            SendMailToSubscribedVolunteerHandler sendMailToSubscribedVolunteerHandler,
            GetAllAnimalsHandler getAllAnimalsHandler
    ) {
        this.sendMailToSubscribedVolunteerHandler = sendMailToSubscribedVolunteerHandler;
        this.getAllAnimalsHandler = getAllAnimalsHandler;
    }

    @Override
    public void run() {
        System.out.println(new Date() + " Launching newsletter task");
        //TODO Add logic here
        //logic might be (if that task is called several times a day):
        //check when newsletter was last sent
        //if that period exceeds what is defined
        //      perform full process (send all mails)
        //      then update new date for last newsletter execution
        // TODO: Ajouter la logique de base

        //List<AnimalEntity> animals = this.getAllAnimalsHandler.handle(null);
        //System.out.println(new Date().toString() + this.sendMailToSubscribedVolunteerHandler.handle(animals));

    }
}
