package esgi.hessepa.infrastructure.web.modelRequests;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public final class PostVolunteerRequest
{
    @NotNull @NotEmpty
    public String name;
    @NotNull @NotEmpty
    public String mail;
    @NotNull @NotEmpty
    public String password;

    public String job;
    public String pictureUrl;
    public boolean isSubscribedToNewsletters;
}
