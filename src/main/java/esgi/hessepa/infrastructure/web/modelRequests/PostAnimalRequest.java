package esgi.hessepa.infrastructure.web.modelRequests;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public final class PostAnimalRequest
{
    @NotNull @NotEmpty
    public String name;
    @NotNull @NotEmpty
    public String species;

    public String description;
    public String pictureUrl;
}
