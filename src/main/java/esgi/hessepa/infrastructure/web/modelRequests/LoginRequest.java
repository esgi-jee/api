package esgi.hessepa.infrastructure.web.modelRequests;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public final class LoginRequest
{
    @NotNull @NotEmpty
    public String mail;
    @NotNull @NotEmpty
    public String password;
}
