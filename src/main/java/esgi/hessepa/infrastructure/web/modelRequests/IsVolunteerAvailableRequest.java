package esgi.hessepa.infrastructure.web.modelRequests;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public final class IsVolunteerAvailableRequest
{
    @NotNull @NotEmpty
    public String mail;
    @NotNull
    public LocalDateTime startDate;
    @NotNull
    public LocalDateTime endDate;
}
