package esgi.hessepa.infrastructure.web.modelRequests;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public final class CreateMissionRequest
{
    @NotNull @NotEmpty
    public String description;
    @NotNull
    public List<String> animalIds;
    @NotNull
    public List<String> volunteerMails;
    @NotNull
    public LocalDateTime startDate;
    @NotNull
    public LocalDateTime endDate;
}
