package esgi.hessepa.infrastructure.web.modelRequests;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public final class SubscribeToNewslettersRequest {
    @NotNull
    @NotEmpty
    public String id;

    @NotNull
    public Boolean wantToSubscribe;
}
