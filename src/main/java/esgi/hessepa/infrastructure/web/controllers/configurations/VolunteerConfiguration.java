package esgi.hessepa.infrastructure.web.controllers.configurations;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public final class VolunteerConfiguration
{
    public boolean isMailValid(String mailAddress)
    {
        return Pattern.compile("^(.+)@(\\S+)$")
                .matcher(mailAddress)
                .matches();
    }

    public boolean isPasswordValid(String password)
    {
        return Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{4,}$")
                .matcher(password)
                .matches();
    }
}

@Configuration
class SpringConfiguration
{
    /*
    @Bean
    public JpaRepo<Volunteer> volunteerRepository()
    {
        return new Repository<>();
    }

    @Bean
    public JpaRepo<Animal> animalRepository()
    {
        return new Repository<>();
    }
    */

    @Bean
    public LevenshteinDistance levenshteinDistance()
    {
        return new LevenshteinDistance();
    }
}