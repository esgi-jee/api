package esgi.hessepa.infrastructure.web.controllers;

import esgi.hessepa.application.dtos.mission.SaveMissionDto;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.domain.models.Mission;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.jpaRepositories.EntityNotFoundException;
import esgi.hessepa.infrastructure.spring.SpringHandlers;
import esgi.hessepa.infrastructure.web.modelRequests.CreateMissionRequest;
import esgi.hessepa.infrastructure.web.modelResources.MissionResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/missions")
public class MissionController {

    private final SpringHandlers handlers;

    public MissionController(SpringHandlers handlers) {
        this.handlers = handlers;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createMission(@Valid @RequestBody CreateMissionRequest request)
    {
        try {
            List<EntityId> animalsIds = new ArrayList<>();
            request.animalIds.forEach(animalId -> animalsIds.add(EntityId.fromString(animalId)));

            EntityId missionId = this.handlers.saveMission().handle(new SaveMissionDto(
                    request.description,
                    animalsIds,
                    request.volunteerMails,
                    request.startDate,
                    request.endDate
            ));

            return ResponseEntity
                    .created(linkTo(methodOn(MissionController.class).getMissions(missionId.toString())).toUri())
                    .build();
        } catch (IllegalArgumentException | HandlerException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<?> getMissions(@RequestParam(defaultValue = "") String id)
    {
        if (id != null && !id.trim().isEmpty())
        {
            try {
                MissionResource mission = asResource(this.handlers.getByIdMission().handle(EntityId.fromString(id)));

                return ResponseEntity.ok(mission);

            } catch (EntityNotFoundException e) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
            }
        }

        List<MissionResource> missions = this.handlers.getAllMissions().handle(null).stream()
                .map(MissionController::asResource)
                .collect(Collectors.toList());

        return ResponseEntity.ok(missions);
    }

    @DeleteMapping
    public ResponseEntity<?> completeMission(String id)
    {
        try {
            this.handlers.completeMission().handle(EntityId.fromString(id));

            return ResponseEntity.ok("Mission complete");

        } catch (HandlerException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public static MissionResource asResource(Mission mission)
    {
        return new MissionResource(
                mission.getId().toString(),
                mission.getDescription(),
                mission.getAnimals().stream()
                        .map(m -> linkTo(methodOn(AnimalController.class).getAnimals(m.getId().toString())).withRel("animal"))
                        .collect(Collectors.toList()),
                mission.getVolunteers().stream()
                        .map(m -> linkTo(methodOn(VolunteerController.class).getVolunteers(m.getMail())).withRel("volunteer"))
                        .collect(Collectors.toList()),
                mission.getPeriod().getStartDate(),
                mission.getPeriod().getEndDate()
        );
    }

}
