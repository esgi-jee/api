package esgi.hessepa.infrastructure.web.controllers;

import esgi.hessepa.application.dtos.volunteer.IsVolunteerAvailableDto;
import esgi.hessepa.application.dtos.volunteer.SaveVolunteerDto;
import esgi.hessepa.application.dtos.volunteer.SubscribeToNewLettersDto;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.spring.SpringHandlers;
import esgi.hessepa.infrastructure.jpa.jpaRepositories.EntityNotFoundException;
import esgi.hessepa.infrastructure.web.modelRequests.IsVolunteerAvailableRequest;
import esgi.hessepa.infrastructure.web.modelRequests.PostVolunteerRequest;
import esgi.hessepa.infrastructure.web.modelRequests.SubscribeToNewslettersRequest;
import esgi.hessepa.infrastructure.web.modelResources.VolunteerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/volunteers")
public class VolunteerController
{
    @Autowired
    private Environment environment;

    private final SpringHandlers handlers;

    public VolunteerController(SpringHandlers handlers)
    {
        this.handlers = handlers;
    }

    @GetMapping(value = "/bonjour")
    public ResponseEntity<String> hello()
    {
        return ResponseEntity.ok("Hello");
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createUser(@Valid @RequestBody PostVolunteerRequest request)
    {
        try {
            String mail = this.handlers.saveVolunteer().handle(new SaveVolunteerDto(
                    request.name,
                    request.mail,
                    request.password,
                    request.job,
                    request.pictureUrl,
                    request.isSubscribedToNewsletters
            ));

            // https://stackoverflow.com/questions/5506005/play-framework-double-url-encoding
            URI location = linkTo(methodOn(VolunteerController.class).getVolunteers(mail)).toUri();
            String locationDebug = location.toString().replace("%2540", "@");

            return ResponseEntity
                    .created(new URI(locationDebug))
                    .build();

        } catch (HandlerException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping
    public ResponseEntity<?> getVolunteers(@RequestParam(defaultValue = "") String mail)
    {
        if (mail != null && !mail.trim().isEmpty())
        {
            try {
                mail = mail.replace("%40", "@");

                VolunteerResource volunteer = asResource(this.handlers.getByMailVolunteer().handle(mail));

                return ResponseEntity.ok(volunteer);

            } catch (EntityNotFoundException e) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
            }
        }

        List<VolunteerResource> volunteers = this.handlers.getAllVolunteers().handle(null).stream()
                .map(VolunteerController::asResource)
                .collect(Collectors.toList());

        return ResponseEntity.ok(volunteers);
    }

    @GetMapping(value = "/name")
    public ResponseEntity<?> getVolunteersByName(@RequestParam String name)
    {
        try {
            List<VolunteerResource> volunteers = this.handlers.getByNameVolunteers().handle(name).stream()
                    .map(VolunteerController::asResource)
                    .collect(Collectors.toList());

            return ResponseEntity.status(HttpStatus.OK).body(volunteers);

        } catch (HandlerException e) {return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteByMail(@RequestParam String mail)
    {
        try {
            Volunteer volunteer = this.handlers.getByMailVolunteer().handle(mail.replace("%40", "@"));

            this.handlers.deleteVolunteer().handle(volunteer);

            return ResponseEntity.ok().build();

        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> subscribeToNewsLetters(@Valid @RequestBody SubscribeToNewslettersRequest request)
    {
        try {
            Volunteer volunteer = this.handlers.subscribeToNewsletters().handle(new SubscribeToNewLettersDto(
                        request.id,
                        request.wantToSubscribe
                    ));

            return ResponseEntity.ok(volunteer.getMail());

        } catch (HandlerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PutMapping(value = "/available")
    public ResponseEntity<?> checkIfVolunteerIsAvailable(@Valid @RequestBody IsVolunteerAvailableRequest request)
    {
        try {
            Boolean isAvailable = this.handlers.isVolunteerAvailable().handle(new IsVolunteerAvailableDto(
                    request.mail,
                    request.startDate,
                    request.endDate));

            return ResponseEntity.ok(!isAvailable);

        } catch (HandlerException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public static VolunteerResource asResource(Volunteer volunteer)
    {
        return new VolunteerResource(
                volunteer.getName(),
                volunteer.getMail(),
                volunteer.getJob(),
                volunteer.getPictureUrl(),
                volunteer.getMissions().stream()
                        .map(m -> linkTo(methodOn(MissionController.class).getMissions(m.getId().toString())).withRel("mission"))
                        .collect(Collectors.toList())
        );
    }
}
