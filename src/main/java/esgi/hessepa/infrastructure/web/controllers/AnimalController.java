package esgi.hessepa.infrastructure.web.controllers;

import esgi.hessepa.application.dtos.animal.SaveAnimalDto;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.jpaRepositories.EntityNotFoundException;
import esgi.hessepa.infrastructure.spring.SpringHandlers;
import esgi.hessepa.infrastructure.web.modelRequests.PostAnimalRequest;
import esgi.hessepa.infrastructure.web.modelResources.AnimalResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/animals")
public class AnimalController
{
    private final SpringHandlers handlers;

    public AnimalController(SpringHandlers handlers)
    {
        this.handlers = handlers;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createAnimal(@Valid @RequestBody PostAnimalRequest request)
    {
        try {
            EntityId animalId = this.handlers.saveAnimal().handle(new SaveAnimalDto(
                    request.name,
                    request.species,
                    request.description,
                    request.pictureUrl
            ));

            return ResponseEntity
                    .created(linkTo(methodOn(AnimalController.class).getAnimals(animalId.toString())).toUri())
                    .build();

        } catch (HandlerException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<?> getAnimals(@RequestParam(defaultValue = "") String id)
    {
        if (id != null && !id.trim().isEmpty())
        {
            try {
                AnimalResource animal = asResource(this.handlers.getByIdAnimal().handle(EntityId.fromString(id)));

                return ResponseEntity.ok(animal);

            } catch (EntityNotFoundException e) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
            }
        }

        List<AnimalResource> animals = this.handlers.getAllAnimals().handle(null).stream()
                .map(AnimalController::asResource)
                .collect(Collectors.toList());

        return ResponseEntity.ok(animals);
    }

    public static AnimalResource asResource(Animal animal)
    {
        return new AnimalResource(
                animal.getId().toString(),
                animal.getName(),
                animal.getSpecies().value,
                animal.getDescription(),
                animal.getPictureUrl()
        );
    }
}
