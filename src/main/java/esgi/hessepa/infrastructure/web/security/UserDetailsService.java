package esgi.hessepa.infrastructure.web.security;

import esgi.hessepa.application.handlers.volunteer.GetAllVolunteersHandler;
import esgi.hessepa.domain.models.Role;
import esgi.hessepa.domain.models.RoleEnum;
import esgi.hessepa.domain.models.Volunteer;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService
{
    private final GetAllVolunteersHandler getAllVolunteersHandler;

    public UserDetailsService(GetAllVolunteersHandler getAllVolunteersHandler)
    {
        this.getAllVolunteersHandler = getAllVolunteersHandler;
    }

    @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException
    {
        Volunteer volunteer = this.getAllVolunteersHandler.handle(null).stream()
                .filter(v -> v.getMail().equals(mail))
                .findFirst()
                .orElse(null);

        if (volunteer == null)
        {
            throw new UsernameNotFoundException("User not found");
        }

        return new User(
                volunteer.getMail(),
                volunteer.getPassword(),
                List.of(new SimpleGrantedAuthority(new Role(RoleEnum.valueOf(volunteer.getRole())).getAuthority()))
        );
    }
}
