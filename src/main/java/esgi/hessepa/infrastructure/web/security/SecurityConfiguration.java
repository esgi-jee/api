package esgi.hessepa.infrastructure.web.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ObjectMapper objectMapper;

    @Value("${jwt.secret-token}")
    private String jwtSecret;

    public SecurityConfiguration(
            UserDetailsService userDetailsService,
            BCryptPasswordEncoder bCryptPasswordEncoder,
            ObjectMapper objectMapper)
    {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.objectMapper = objectMapper;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authManager) throws Exception
    {
        authManager
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(this.bCryptPasswordEncoder);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(STATELESS);
        http.cors().and().authorizeRequests().antMatchers("/resource").permitAll();

        http.authorizeRequests().antMatchers(GET, "/volunteers/**").hasAnyAuthority("[VOLUNTEER]", "[ADMIN]");
        http.authorizeRequests().antMatchers(DELETE, "/volunteers/**").hasAnyAuthority("[ADMIN]");
        http.authorizeRequests().antMatchers(POST, "/volunteers/**").permitAll();

        http.authorizeRequests().antMatchers(GET, "/missions/**").permitAll();
        http.authorizeRequests().antMatchers(DELETE, "/missions/**").permitAll();
        http.authorizeRequests().antMatchers(POST, "/missions/**").permitAll();

        http.authorizeRequests().antMatchers(GET, "/animals/**").permitAll();
        http.authorizeRequests().antMatchers(DELETE, "/animals/**").permitAll();
        http.authorizeRequests().antMatchers(POST, "/animals/**").permitAll();

        http.authorizeRequests().antMatchers("/test-fixtures/**").permitAll();

        http.authorizeRequests().anyRequest().authenticated();

        http.addFilter(new AuthenticationFilter(this.authenticationManager(), this.jwtSecret));
        http.addFilterBefore(new AuthorizationFilter(this.objectMapper, this.jwtSecret), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception
    {
        return super.authenticationManagerBean();
    }
}