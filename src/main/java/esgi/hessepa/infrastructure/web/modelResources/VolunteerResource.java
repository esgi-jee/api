package esgi.hessepa.infrastructure.web.modelResources;

import esgi.hessepa.domain.valueObjects.EntityId;
import lombok.Getter;
import org.springframework.hateoas.Link;

import java.util.List;

public final class VolunteerResource
{
    @Getter private final String name;
    @Getter private final String mail;
    @Getter private final String job;
    @Getter private final String pictureUrl;
    @Getter private final List<Link> missions;

    public VolunteerResource(
            String name,
            String mail,
            String job,
            String pictureUrl,
            List<Link> missions)
    {
        this.name = name;
        this.mail = mail;
        this.job = job;
        this.pictureUrl = pictureUrl;
        this.missions = missions;
    }
}
