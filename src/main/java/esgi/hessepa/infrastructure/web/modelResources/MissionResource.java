package esgi.hessepa.infrastructure.web.modelResources;

import lombok.Getter;
import org.springframework.hateoas.Link;

import java.time.LocalDateTime;
import java.util.List;

public final class MissionResource
{
    @Getter
    private final String id;
    @Getter
    private final String description;
    @Getter
    private final List<Link> animals;
    @Getter
    private final List<Link> volunteers;

    @Getter
    private final LocalDateTime startDate;
    @Getter
    private final LocalDateTime endDate;

    public MissionResource(
            String id,
            String description,
            List<Link> animals,
            List<Link> volunteers,
            LocalDateTime startDate,
            LocalDateTime endDate)
    {
        this.id = id;
        this.description = description;
        this.animals = animals;
        this.volunteers = volunteers;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
