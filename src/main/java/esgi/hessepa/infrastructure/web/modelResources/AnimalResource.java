package esgi.hessepa.infrastructure.web.modelResources;

import lombok.Getter;

public final class AnimalResource
{
    @Getter private final String id;
    @Getter private final String name;
    @Getter private final String species;
    @Getter private final String description;
    @Getter private final String pictureUrl;

    public AnimalResource(
            String id,
            String name,
            String species,
            String description,
            String imageUrl)
    {
        this.id = id;
        this.name = name;
        this.species = species;
        this.description = description;
        this.pictureUrl = imageUrl;
    }
}
