package esgi.hessepa.infrastructure.jpa.entities;

import esgi.hessepa.domain.models.Job;
import esgi.hessepa.domain.models.RoleEnum;
import lombok.Getter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Volunteers")
public final class VolunteerEntity
{
    @Id
    @Getter private String id;

    @Getter private String mail;
    @Getter private String password;
    @Getter private String name;
    @Getter private String job;
    @Getter private String role;
    @Getter private String pictureUrl;
    @Getter private boolean isSubscribedToNewsletters;

    @ManyToMany(fetch = FetchType.EAGER )
    @Getter private Set<MissionEntity> missions;

    public VolunteerEntity()
    {

    }

    public VolunteerEntity(
            String id,
            String mail,
            String password,
            String name,
            Job job,
            String pictureUrl,
            RoleEnum role,
            Set<MissionEntity> missions,
            boolean isSubscribedToNewsletters)
    {
        this.id = id;
        this.mail = mail;
        this.password = password;
        this.name = name;
        this.job = job.value;
        this.pictureUrl = pictureUrl;
        this.role = role.value;
        this.missions = missions;
        this.isSubscribedToNewsletters = isSubscribedToNewsletters;
    }
}
