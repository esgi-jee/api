package esgi.hessepa.infrastructure.jpa.entities;

import lombok.Getter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Animals")
public final class AnimalEntity
{
    @Id
    @Getter private String id;
    @Getter private String name;
    @Getter private String species;
    @Getter private String description;
    @Getter private String pictureUrl;
    @ManyToMany(fetch = FetchType.EAGER )
    @Getter private Set<MissionEntity> missions;

    public AnimalEntity()
    {

    }

    public AnimalEntity(String id, String name, String species, String description, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.species = species;
        this.description = description;
        this.pictureUrl = pictureUrl;
    }
}
