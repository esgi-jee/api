package esgi.hessepa.infrastructure.jpa.entities;

import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "Missions")
public final class MissionEntity
{
    @Id
    @Getter private String id;
    @Getter private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @Getter private Set<AnimalEntity> animals;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Getter private Set<VolunteerEntity> volunteers;

    @Getter private LocalDateTime startDate;
    @Getter private LocalDateTime endDate;

    public MissionEntity()
    {

    }

    public MissionEntity(
            String id,
            String description,
            Set<AnimalEntity> animals,
            Set<VolunteerEntity> volunteers,
            LocalDateTime startDate,
            LocalDateTime endDate)
    {
        this.id = id;
        this.description = description;
        this.animals = animals;
        this.volunteers = volunteers;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
