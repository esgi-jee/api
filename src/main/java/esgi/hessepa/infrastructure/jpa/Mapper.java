package esgi.hessepa.infrastructure.jpa;

import esgi.hessepa.domain.models.*;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.domain.valueObjects.WindowedPeriod;
import esgi.hessepa.infrastructure.jpa.entities.AnimalEntity;
import esgi.hessepa.infrastructure.jpa.entities.MissionEntity;
import esgi.hessepa.infrastructure.jpa.entities.VolunteerEntity;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public final class Mapper
{
    public MissionEntity missionDomainToEntity(Mission mission)
    {
        return new MissionEntity(
                mission.getId().toString(),
                mission.getDescription(),
                mission.getAnimals().stream()
                        .map(this::animaDomainToEntity)
                        .collect(Collectors.toSet()),
                mission.getVolunteers().stream()
                        .map(this::volunteerDomainToEntity)
                        .collect(Collectors.toSet()),
                mission.getPeriod().getStartDate(),
                mission.getPeriod().getEndDate()
        );
    }

    public Mission missionEntityToDomain(MissionEntity missionEntity)
    {
        return Mission.of(
                EntityId.fromString(missionEntity.getId()),
                missionEntity.getDescription(),
                missionEntity.getAnimals().stream()
                        .map(this::animalEntityToDomain)
                        .collect(Collectors.toSet()),
                missionEntity.getVolunteers().stream()
                        .map(this::volunteerEntityToDomain)
                        .collect(Collectors.toSet()),
                new WindowedPeriod(missionEntity.getStartDate(), missionEntity.getEndDate())
        );
    }

    private MissionEntity missionDomainToEntityWithoutVolunteers(Mission mission)
    {
        return new MissionEntity(
                mission.getId().toString(),
                mission.getDescription(),
                mission.getAnimals().stream()
                        .map(this::animaDomainToEntity)
                        .collect(Collectors.toSet()),
                null,
                mission.getPeriod().getStartDate(),
                mission.getPeriod().getEndDate()
        );
    }

    private Mission missionEntityToDomainWithoutVolunteers(MissionEntity missionEntity)
    {
        return Mission.of(
                EntityId.fromString(missionEntity.getId()),
                missionEntity.getDescription(),
                missionEntity.getAnimals().stream()
                        .map(this::animalEntityToDomain)
                        .collect(Collectors.toSet()),
                null,
                new WindowedPeriod(missionEntity.getStartDate(), missionEntity.getEndDate())
        );
    }

    public VolunteerEntity volunteerDomainToEntity(Volunteer volunteer)
    {
        return new VolunteerEntity(
                volunteer.getId().toString(),
                volunteer.getMail(),
                volunteer.getPassword(),
                volunteer.getName(),
                Job.valueOf(volunteer.getJob()),
                volunteer.getPictureUrl(),
                RoleEnum.valueOf(volunteer.getRole()),
                volunteer.getMissions().stream()
                        .map(this::missionDomainToEntityWithoutVolunteers)
                        .collect(Collectors.toSet()),
                volunteer.isSubscribedToNewsletters()
        );
    }

    public Volunteer volunteerEntityToDomain(VolunteerEntity volunteerEntity)
    {
        return Volunteer.of(
                EntityId.fromString(volunteerEntity.getId()),
                volunteerEntity.getMail(),
                volunteerEntity.getPassword(),
                volunteerEntity.getName(),
                volunteerEntity.getRole(),
                volunteerEntity.getJob(),
                volunteerEntity.getPictureUrl(),
                volunteerEntity.getMissions().stream()
                        .map(this::missionEntityToDomainWithoutVolunteers)
                        .collect(Collectors.toSet()),
                volunteerEntity.isSubscribedToNewsletters()
        );
    }

    private VolunteerEntity volunteerDomainToEntityWithoutMissions(Volunteer volunteer)
    {
        return new VolunteerEntity(
                volunteer.getId().toString(),
                volunteer.getMail(),
                volunteer.getPassword(),
                volunteer.getName(),
                Job.valueOf(volunteer.getJob()),
                volunteer.getPictureUrl(),
                RoleEnum.valueOf(volunteer.getRole()),
                null,
                volunteer.isSubscribedToNewsletters()
        );
    }

    private Volunteer volunteerEntityToDomainWithoutMissions(VolunteerEntity volunteerEntity)
    {
        return Volunteer.of(
                EntityId.fromString(volunteerEntity.getId()),
                volunteerEntity.getMail(),
                volunteerEntity.getPassword(),
                volunteerEntity.getName(),
                volunteerEntity.getRole(),
                volunteerEntity.getJob(),
                volunteerEntity.getPictureUrl(),
                null,
                volunteerEntity.isSubscribedToNewsletters()
        );
    }

    public AnimalEntity animaDomainToEntity(Animal animal)
    {
        return new AnimalEntity(
                animal.getId().toString(),
                animal.getName(),
                animal.getSpecies().value,
                animal.getDescription(),
                animal.getPictureUrl()
        );
    }

    public Animal animalEntityToDomain(AnimalEntity animalEntity)
    {
        return Animal.of(
                EntityId.fromString(animalEntity.getId()),
                animalEntity.getName(),
                Species.of(animalEntity.getSpecies()),
                animalEntity.getDescription(),
                animalEntity.getPictureUrl()
        );
    }
}
