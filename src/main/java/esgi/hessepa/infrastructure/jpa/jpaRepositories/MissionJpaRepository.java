package esgi.hessepa.infrastructure.jpa.jpaRepositories;

import esgi.hessepa.kernel.repositories.MissionRepositoryInterface;
import esgi.hessepa.domain.models.Mission;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.Mapper;
import esgi.hessepa.kernel.jpaRepositories.JpaMissionRepo;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@Primary
public class MissionJpaRepository implements MissionRepositoryInterface
{
    private final JpaMissionRepo repo;
    private final Mapper mapper;

    public MissionJpaRepository(JpaMissionRepo repo, Mapper mapper)
    {
        this.repo = repo;
        this.mapper = mapper;
    }

    @Override
    public void save(Mission entity)
    {
        this.repo.save(this.mapper.missionDomainToEntity(entity));
    }

    @Override
    public List<Mission> getAll()
    {
        return this.repo.findAll().stream()
                .map(this.mapper::missionEntityToDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Mission getById(EntityId id)
    {
        return this.mapper.missionEntityToDomain(this.repo.getById(id.toString()));
    }

    @Override
    public void removeAll()
    {
        this.repo.deleteAll();
    }

    @Override
    public void removeById(EntityId id)
    {
        this.repo.deleteById(id.toString());
    }
}
