package esgi.hessepa.infrastructure.jpa.jpaRepositories;

import esgi.hessepa.kernel.repositories.AnimalRepositoryInterface;
import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.*;
import esgi.hessepa.kernel.jpaRepositories.JpaAnimalRepo;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@Primary
public class AnimalJpaRepository implements AnimalRepositoryInterface
{
    private final JpaAnimalRepo repo;
    private final Mapper mapper;

    public AnimalJpaRepository(JpaAnimalRepo repo, Mapper mapper)
    {
        this.repo = repo;
        this.mapper = mapper;
    }

    @Override
    public void save(Animal entity)
    {
        this.repo.save(this.mapper.animaDomainToEntity(entity));
    }

    @Override
    public List<Animal> getAll()
    {
        return this.repo.findAll().stream()
                .map(this.mapper::animalEntityToDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Animal getById(EntityId id)
    {
        return this.mapper.animalEntityToDomain(this.repo.getById(id.toString()));
    }

    @Override
    public void removeAll()
    {
        this.repo.deleteAll();
    }

    @Override
    public void removeById(EntityId id)
    {
        this.repo.deleteById(id.toString());
    }
}
