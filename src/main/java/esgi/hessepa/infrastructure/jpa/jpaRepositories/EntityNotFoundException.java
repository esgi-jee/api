package esgi.hessepa.infrastructure.jpa.jpaRepositories;

import esgi.hessepa.domain.valueObjects.EntityId;

public final class EntityNotFoundException extends RuntimeException
{
    public EntityNotFoundException(EntityId id)
    {
        super("Error: entity " + id + " not found");
    }
}
