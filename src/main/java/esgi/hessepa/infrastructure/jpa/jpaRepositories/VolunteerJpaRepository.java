package esgi.hessepa.infrastructure.jpa.jpaRepositories;

import esgi.hessepa.infrastructure.jpa.entities.VolunteerEntity;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.Mapper;
import esgi.hessepa.kernel.jpaRepositories.JpaVolunteerRepo;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@Primary
public class VolunteerJpaRepository implements VolunteerRepositoryInterface
{
    private final JpaVolunteerRepo repo;
    private final Mapper mapper;

    public VolunteerJpaRepository(JpaVolunteerRepo repo, Mapper mapper)
    {
        this.repo = repo;
        this.mapper = mapper;
    }

    @Override
    public void save(Volunteer entity)
    {
        this.repo.save(this.mapper.volunteerDomainToEntity(entity));
    }

    @Override
    public List<Volunteer> getAll()
    {
        return this.repo.findAll().stream()
                .map(this.mapper::volunteerEntityToDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Volunteer getById(EntityId id)
    {
        return this.mapper.volunteerEntityToDomain(this.repo.getById(id.toString()));
    }

    @Override
    public void removeAll()
    {
        this.repo.deleteAll();
    }

    @Override
    public void removeById(EntityId id)
    {
        this.repo.deleteById(id.toString());
    }

    @Override
    public void delete(Volunteer volunteer) {
        this.repo.delete(this.mapper.volunteerDomainToEntity(volunteer));
    }
}
