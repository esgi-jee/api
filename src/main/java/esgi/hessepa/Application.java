package esgi.hessepa;

import esgi.hessepa.infrastructure.tasks.TaskScheduler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@ConfigurationPropertiesScan
public class Application
{
    public static void main(String[] args)
    {
        final ConfigurableApplicationContext applicationContext = SpringApplication.run(Application.class, args);
        // --- TaskScheduler (for newsletter)
        TaskScheduler taskScheduler = applicationContext.getBean(TaskScheduler.class);
        taskScheduler.start();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }
}
