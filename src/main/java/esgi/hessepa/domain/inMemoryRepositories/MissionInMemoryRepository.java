package esgi.hessepa.domain.inMemoryRepositories;

import esgi.hessepa.domain.models.Mission;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.kernel.repositories.MissionRepositoryInterface;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MissionInMemoryRepository implements MissionRepositoryInterface
{
    private final Map<EntityId, Mission> memory = new HashMap<>();

    @Override
    public void save(Mission entity)
    {
        this.memory.put(entity.getId(), entity);
    }

    @Override
    public List<Mission> getAll()
    {
        return new ArrayList<>(this.memory.values());
    }

    @Override
    public Mission getById(EntityId id)
    {
        return this.memory.get(id);
    }

    @Override
    public void removeAll()
    {
        this.memory.clear();
    }

    @Override
    public void removeById(EntityId id)
    {
        this.memory.remove(id);
    }
}
