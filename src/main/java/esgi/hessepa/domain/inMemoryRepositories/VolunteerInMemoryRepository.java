package esgi.hessepa.domain.inMemoryRepositories;

import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.entities.VolunteerEntity;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class VolunteerInMemoryRepository implements VolunteerRepositoryInterface
{
    private final Map<EntityId, Volunteer> memory = new HashMap<>();

    @Override
    public void save(Volunteer entity)
    {
        this.memory.put(entity.getId(), entity);
    }

    @Override
    public List<Volunteer> getAll()
    {
        return new ArrayList<>(this.memory.values());
    }

    @Override
    public Volunteer getById(EntityId id)
    {
        return this.memory.get(id);
    }

    @Override
    public void removeAll()
    {
        this.memory.clear();
    }

    @Override
    public void removeById(EntityId id)
    {
        this.memory.remove(id);
    }

    @Override
    public void delete(Volunteer dto) {
        this.memory.remove(dto.getId());
    }
}
