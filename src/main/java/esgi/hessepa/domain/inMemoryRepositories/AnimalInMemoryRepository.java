package esgi.hessepa.domain.inMemoryRepositories;

import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.kernel.repositories.AnimalRepositoryInterface;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AnimalInMemoryRepository implements AnimalRepositoryInterface
{
    private final Map<EntityId, Animal> memory = new HashMap<>();

    @Override
    public void save(Animal entity)
    {
        this.memory.put(entity.getId(), entity);
    }

    @Override
    public List<Animal> getAll()
    {
        return new ArrayList<>(this.memory.values());
    }

    @Override
    public Animal getById(EntityId id)
    {
        return this.memory.get(id);
    }

    @Override
    public void removeAll()
    {
        this.memory.clear();
    }

    @Override
    public void removeById(EntityId id)
    {
        this.memory.remove(id);
    }
}
