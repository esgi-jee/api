package esgi.hessepa.domain.valueObjects;

import java.util.Objects;
import java.util.UUID;

public final class EntityId
{
    private final String id;

    private EntityId(String id)
    {
        this.id = id;
    }

    public static EntityId generate()
    {
        return new EntityId(UUID.randomUUID().toString());
    }

    public static EntityId fromString(String idStr)
    {
        return new EntityId(idStr);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityId id = (EntityId) o;
        return Objects.equals(this.id, id.id);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public String toString()
    {
        return this.id;
    }
}
