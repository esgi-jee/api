package esgi.hessepa.domain.models;

import esgi.hessepa.domain.valueObjects.EntityId;
import lombok.Getter;

public abstract class Entity
{
    @Getter private final EntityId id;

    protected Entity(EntityId id)
    {
        this.id = id;
    }
}
