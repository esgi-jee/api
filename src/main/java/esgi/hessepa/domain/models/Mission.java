package esgi.hessepa.domain.models;

import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.domain.valueObjects.WindowedPeriod;
import lombok.Getter;

import java.util.Set;

public final class Mission extends Entity {
    @Getter
    private final String description;

    @Getter
    private final Set<Animal> animals;
    @Getter
    private final Set<Volunteer> volunteers;

    @Getter
    private final WindowedPeriod period;

    private Mission(
            EntityId id,
            String description,
            Set<Animal> animals,
            Set<Volunteer> volunteers,
            WindowedPeriod period) {
        super(id);
        this.description = description;
        this.animals = animals;
        this.volunteers = volunteers;
        this.period = period;
    }

    public static Mission of(
            EntityId id,
            String description,
            Set<Animal> animals,
            Set<Volunteer> volunteers,
            WindowedPeriod period) {
        return new Mission(
                id,
                description,
                animals,
                volunteers,
                period
        );
    }

    public boolean overlapsWith(WindowedPeriod otherPeriod) {
        return this.period.overlapsWith(otherPeriod);
    }
}
