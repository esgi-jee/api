package esgi.hessepa.domain.models;

import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.domain.valueObjects.WindowedPeriod;
import lombok.Getter;

import java.util.Set;

public final class Volunteer extends Entity
{
    @Getter private final String mail;
    @Getter private final String password;
    @Getter private final String name;
    @Getter private final String role;
    @Getter private final String job;
    @Getter private final String pictureUrl;
    @Getter private final Set<Mission> missions;
    @Getter private final boolean isSubscribedToNewsletters;

    private Volunteer(
            EntityId id,
            String mail,
            String password,
            String name,
            String role,
            String job,
            String pictureUrl,
            Set<Mission> missions,
            boolean isSubscribedToNewsletters)
    {
        super(id);
        this.mail = mail;
        this.password = password;
        this.name = name;
        this.role = role;
        this.job = job;
        this.pictureUrl = pictureUrl;
        this.missions = missions;
        this.isSubscribedToNewsletters = isSubscribedToNewsletters;
    }

    public static Volunteer of(
            String mail,
            String password,
            String name,
            String job,
            String pictureUrl,
            Set<Mission> missions,
            boolean isSubscribedToNewsletters)
    {
        return new Volunteer(
                EntityId.generate(),
                mail,
                password,
                name,
                RoleEnum.VOLUNTEER.toString(),
                job,
                pictureUrl,
                missions,
                isSubscribedToNewsletters);
    }

    public static Volunteer of(
            EntityId id,
            String mail,
            String password,
            String name,
            String role,
            String job,
            String pictureUrl,
            Set<Mission> missions,
            boolean isSubscribedToNewsletters)
    {
        return new Volunteer(
                id,
                mail,
                password,
                name,
                role,
                job,
                pictureUrl,
                missions,
                isSubscribedToNewsletters);
    }

    public boolean isAvailableFor(WindowedPeriod period)
    {
        for (Mission mission : missions)
        {
            if (mission.overlapsWith(period))
                return false;
        }
        return true;
    }
}
