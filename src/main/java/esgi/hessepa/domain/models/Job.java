package esgi.hessepa.domain.models;

import esgi.hessepa.domain.models.exceptions.UnknownJobException;

import java.util.Arrays;

public enum Job
{
    GROOMER("GROOMER"),
    VETERINARIAN("VETERINARIAN"),
    ADMIN("ADMIN"),

    UNSPECIFIED("UNSPECIFIED");

    public final String value;

    Job(String value)
    {
        this.value = value;
    }

    public static Job of(String jobStr) throws UnknownJobException
    {
        if (jobStr == null || jobStr.isBlank())
        {
            return Job.UNSPECIFIED;
        }

        return Arrays.stream(Job.values())
                .filter(job -> jobStr.equalsIgnoreCase(job.value))
                .findFirst()
                .orElseThrow(() -> new UnknownJobException(jobStr));
    }
}
