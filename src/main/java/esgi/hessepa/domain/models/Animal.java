package esgi.hessepa.domain.models;

import esgi.hessepa.domain.valueObjects.EntityId;
import lombok.Getter;

public final class Animal extends Entity
{
    @Getter private final String name;
    @Getter private final Species species;
    @Getter private final String description;
    @Getter private final String pictureUrl;

    private Animal(
            EntityId id,
            String name,
            Species species,
            String description,
            String pictureUrl)
    {
        super(id);
        this.name = name;
        this.species = species;
        this.description = description;
        this.pictureUrl = pictureUrl;
    }

    public static Animal of(
            String name,
            Species species,
            String description,
            String pictureUrl)
    {
        return new Animal(
                EntityId.generate(),
                name,
                species,
                description,
                pictureUrl
        );
    }

    public static Animal of(
            EntityId id,
            String name,
            Species species,
            String description,
            String pictureUrl)
    {
        return new Animal(
                id,
                name,
                species,
                description,
                pictureUrl
        );
    }
}
