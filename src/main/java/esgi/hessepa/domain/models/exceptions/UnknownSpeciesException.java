package esgi.hessepa.domain.models.exceptions;

public final class UnknownSpeciesException extends RuntimeException
{
    public UnknownSpeciesException(String species)
    {
        super("Error: unknown species " + species);
    }
}