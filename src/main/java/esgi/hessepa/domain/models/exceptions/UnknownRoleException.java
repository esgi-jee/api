package esgi.hessepa.domain.models.exceptions;

public final class UnknownRoleException extends RuntimeException
{
    public UnknownRoleException(String role)
    {
        super("Error: unknown role " + role);
    }
}
