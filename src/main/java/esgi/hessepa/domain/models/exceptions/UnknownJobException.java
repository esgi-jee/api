package esgi.hessepa.domain.models.exceptions;

public final class UnknownJobException extends RuntimeException
{
    public UnknownJobException(String job)
    {
        super("Error: unknown job " + job);
    }
}
