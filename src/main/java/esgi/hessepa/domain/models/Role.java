package esgi.hessepa.domain.models;

import org.springframework.security.core.GrantedAuthority;

public final class Role implements GrantedAuthority
{
    private final RoleEnum role;

    public Role(RoleEnum role)
    {
        this.role = role;
    }

    @Override
    public String getAuthority()
    {
        return this.role.toString();
    }

    public RoleEnum asEnum()
    {
        return this.role;
    }
}
