package esgi.hessepa.domain.models;

import esgi.hessepa.domain.models.exceptions.UnknownSpeciesException;

import java.util.Arrays;

public enum Species
{
    CAT("Cat"),
    DOG("Dog"),
    POKEMON("Pokemon");

    public final String value;

    Species(String value)
    {
        this.value = value;
    }

    public static Species of(String speciesStr) throws UnknownSpeciesException
    {
        if (speciesStr == null || speciesStr.isEmpty())
        {
            throw new UnknownSpeciesException(speciesStr);
        }

        return Arrays.stream(Species.values())
                .filter(species -> speciesStr.equalsIgnoreCase(species.value))
                .findFirst()
                .orElseThrow(() -> new UnknownSpeciesException(speciesStr));
    }
}
