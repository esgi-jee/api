package esgi.hessepa.domain.models;

import esgi.hessepa.domain.models.exceptions.UnknownRoleException;

import java.util.Arrays;

public enum RoleEnum
{
    VOLUNTEER("VOLUNTEER"),
    ADMIN("ADMIN");

    public final String value;

    RoleEnum(String value)
    {
        this.value = value;
    }

    public static RoleEnum of(String roleStr) throws UnknownRoleException
    {
        if (roleStr == null || roleStr.isEmpty())
        {
            throw new UnknownRoleException(roleStr);
        }

        return Arrays.stream(RoleEnum.values())
                .filter(role -> roleStr.equalsIgnoreCase(role.value))
                .findFirst()
                .orElseThrow(() -> new UnknownRoleException(roleStr));
    }
}
