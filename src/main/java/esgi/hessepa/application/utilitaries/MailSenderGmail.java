package esgi.hessepa.application.utilitaries;

import esgi.hessepa.domain.models.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

@Service
public class MailSenderGmail {

    @Autowired
    private Environment environment;
    
    public void sendMail(String mail, String volunteerMail) throws MessagingException, UnsupportedEncodingException {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);
        mailSender.setUsername(environment.getProperty("MASTER_MAIL"));
        mailSender.setPassword(environment.getProperty("MASTER_GMAIL_PASSWORD"));

        Properties mailProperties = new Properties();
        mailProperties.setProperty("mail.smtp.auth", "true");
        mailProperties.setProperty("mail.smtp.starttls.enable", "true");

        mailSender.setJavaMailProperties(mailProperties);

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message);

        mimeMessageHelper.setFrom(Objects.requireNonNull(environment.getProperty("MASTER_MAIL")), "HessPA Newsletters");
        mimeMessageHelper.setTo(volunteerMail);
        mimeMessageHelper.setSubject("HessPA, Adoptez les tous !");
        mimeMessageHelper.setText(mail, true);
        mailSender.send(message);

    }

    public String generateMailBody(List<Animal> animals) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Bonjour, voici les animaux dont il faudrait s'occuper: ");
        animals.forEach(animal -> {
            stringBuilder.append(animal.getName()).append(" ");
        });
        return  stringBuilder.toString();
    }
}
