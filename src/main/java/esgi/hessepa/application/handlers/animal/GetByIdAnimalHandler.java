package esgi.hessepa.application.handlers.animal;

import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.AnimalRepositoryInterface;
import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.valueObjects.EntityId;

public final class GetByIdAnimalHandler implements Handler<Animal, EntityId>
{
    private final AnimalRepositoryInterface animalRepository;

    public GetByIdAnimalHandler(AnimalRepositoryInterface animalRepository)
    {
        this.animalRepository = animalRepository;
    }

    @Override
    public Animal handle(EntityId entityId)
    {
        return this.animalRepository.getById(entityId);
    }
}