package esgi.hessepa.application.handlers.animal;

import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.AnimalRepositoryInterface;
import esgi.hessepa.domain.models.Animal;

import java.util.List;

public final class GetAllAnimalsHandler implements Handler<List<Animal>, Void>
{
    private final AnimalRepositoryInterface animalRepository;

    public GetAllAnimalsHandler(AnimalRepositoryInterface animalRepository)
    {
        this.animalRepository = animalRepository;
    }

    @Override
    public List<Animal> handle(Void dto)
    {
        return this.animalRepository.getAll();
    }
}