package esgi.hessepa.application.handlers.animal;

import esgi.hessepa.application.dtos.animal.SaveAnimalDto;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.application.handlers.exception.ErrorCode;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.kernel.repositories.AnimalRepositoryInterface;
import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.models.Species;
import esgi.hessepa.domain.models.exceptions.UnknownSpeciesException;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.apis.AnimalPicturesApi;

import java.io.IOException;

public final class SaveAnimalHandler implements Handler<EntityId, SaveAnimalDto>
{
    private final AnimalRepositoryInterface animalRepository;
    private final AnimalPicturesApi animalPicturesApi;

    public SaveAnimalHandler(
            AnimalRepositoryInterface animalRepository,
            AnimalPicturesApi animalPicturesApi)
    {
        this.animalRepository = animalRepository;
        this.animalPicturesApi = animalPicturesApi;
    }

    @Override
    public EntityId handle(SaveAnimalDto dto)
    {
        try {
            Species species = Species.of(dto.species);

            String url;
            if (dto.pictureUrl.trim().isEmpty())
            {
                url = species == Species.CAT ?
                        this.animalPicturesApi.getCatPictureUrl() :
                        this.animalPicturesApi.getDogPictureUrl();
            }
            else
            {
                url = dto.pictureUrl;
            }

            Animal animal = Animal.of(
                    dto.name,
                    species,
                    dto.description,
                    url
            );

            this.animalRepository.save(animal);

            return animal.getId();

        } catch (UnknownSpeciesException e) {
            throw new HandlerException(ErrorCode.UNKNOWN_SPECIES, dto.species);
        } catch (IOException e) {
            throw new HandlerException(ErrorCode.API_READ_ERROR, e.getMessage());
        }
    }
}
