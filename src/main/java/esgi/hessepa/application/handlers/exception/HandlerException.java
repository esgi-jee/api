package esgi.hessepa.application.handlers.exception;

public final class HandlerException extends RuntimeException
{
    public HandlerException(ErrorCode errorCode, String wrongValue)
    {
        super("Error: " + errorCode.message + " " + wrongValue);
    }
}