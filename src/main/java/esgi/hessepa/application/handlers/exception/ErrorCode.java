package esgi.hessepa.application.handlers.exception;

public enum ErrorCode
{
    ALREADY_REGISTERED_MAIL("mail address already exists"),
    INVALID_MAIL("invalid mail address"),
    PASSWORD("invalid password"),

    UNKNOWN_JOB("unknown job"),
    UNKNOWN_SPECIES("unknown species"),

    API_READ_ERROR("external api read error"),
    NO_MATCHING("no matching found"),
    ERROR_SEND_MAIL("error while sending mail");

    public final String message;

    ErrorCode(String message)
    {
        this.message = message;
    }
}