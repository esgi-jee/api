package esgi.hessepa.application.handlers.mission;

import esgi.hessepa.application.dtos.mission.SaveMissionDto;
import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.models.Mission;
import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.domain.valueObjects.WindowedPeriod;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.AnimalRepositoryInterface;
import esgi.hessepa.kernel.repositories.MissionRepositoryInterface;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;

import java.util.HashSet;
import java.util.Set;

public final class SaveMissionHandler implements Handler<EntityId, SaveMissionDto> {
    private final MissionRepositoryInterface missionRepository;
    private final VolunteerRepositoryInterface volunteerRepository;
    private final AnimalRepositoryInterface animalRepository;

    public SaveMissionHandler(
            MissionRepositoryInterface missionRepository,
            VolunteerRepositoryInterface volunteerRepository,
            AnimalRepositoryInterface animalRepository) {
        this.missionRepository = missionRepository;
        this.volunteerRepository = volunteerRepository;
        this.animalRepository = animalRepository;
    }

    @Override
    public EntityId handle(SaveMissionDto dto)
    {
        WindowedPeriod windowedPeriod = new WindowedPeriod(dto.startDate, dto.endDate);

        Set<Volunteer> volunteers = new HashSet<>();
        dto.volunteers.forEach(volunteerMail ->
                        this.volunteerRepository.getAll().forEach(volunteer -> {
                                if (volunteer.getMail().equals(volunteerMail)) {
                                    volunteers.add(volunteer);
                                }
                            })
        );

        Set<Animal> animals = new HashSet<>();
        dto.animals.forEach(animalId -> animals.add(this.animalRepository.getById(animalId)));

        Mission mission = Mission.of(
                EntityId.generate(),
                dto.description,
                animals,
                volunteers,
                windowedPeriod
        );

        if (mission.getVolunteers().isEmpty()) {
            throw new IllegalArgumentException("No volunteers available for this mission");
        }

        if (mission.getAnimals().isEmpty()) {
            throw new IllegalArgumentException("No animals defined for this mission");
        }

        this.missionRepository.save(mission);

        dto.volunteers.forEach(volunteerMail ->
                this.volunteerRepository.getAll().forEach(volunteer -> {
                    if (volunteer.getMail().equals(volunteerMail)) {
                        volunteer.getMissions().add(mission);
                        this.volunteerRepository.save(volunteer);
                    }
                })
        );

        return mission.getId();
    }
}
