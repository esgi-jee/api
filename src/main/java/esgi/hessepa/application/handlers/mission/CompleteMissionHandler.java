package esgi.hessepa.application.handlers.mission;

import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.MissionRepositoryInterface;

public final class CompleteMissionHandler implements Handler<Void, EntityId> {

    private final MissionRepositoryInterface missionRepository;

    public CompleteMissionHandler(MissionRepositoryInterface missionRepository) {
        this.missionRepository = missionRepository;
    }

    @Override
    public Void handle(EntityId entityId) {
        this.missionRepository.removeById(entityId);
        return null;
    }
}
