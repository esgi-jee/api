package esgi.hessepa.application.handlers.mission;

import esgi.hessepa.domain.models.Mission;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.MissionRepositoryInterface;

public final class GetByIdMissionHandler implements Handler<Mission, EntityId>
{
    private final MissionRepositoryInterface missionRepository;

    public GetByIdMissionHandler(MissionRepositoryInterface missionRepository)
    {
        this.missionRepository = missionRepository;
    }

    @Override
    public Mission handle(EntityId entityId)
    {
        return this.missionRepository.getById(entityId);
    }
}
