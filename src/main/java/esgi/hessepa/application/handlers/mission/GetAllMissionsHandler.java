package esgi.hessepa.application.handlers.mission;

import esgi.hessepa.domain.models.Mission;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.MissionRepositoryInterface;

import java.util.List;

public final class GetAllMissionsHandler implements Handler<List<Mission>, Void>
{
    public GetAllMissionsHandler(MissionRepositoryInterface missionRepository) {
        this.missionRepository = missionRepository;
    }

    private final MissionRepositoryInterface missionRepository;

    @Override
    public List<Mission> handle(Void dto)
    {
        return this.missionRepository.getAll();
    }
}
