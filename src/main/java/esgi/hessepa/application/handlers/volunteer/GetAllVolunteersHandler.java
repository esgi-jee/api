package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.Volunteer;

import java.util.List;

public final class GetAllVolunteersHandler implements Handler<List<Volunteer>, Void>
{
    private final VolunteerRepositoryInterface volunteerRepository;

    public GetAllVolunteersHandler(VolunteerRepositoryInterface volunteerRepository)
    {
        this.volunteerRepository = volunteerRepository;
    }

    @Override
    public List<Volunteer> handle(Void dto)
    {
        return this.volunteerRepository.getAll();
    }
}
