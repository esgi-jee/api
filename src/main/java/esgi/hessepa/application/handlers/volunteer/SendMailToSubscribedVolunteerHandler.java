package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.application.handlers.exception.ErrorCode;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.application.utilitaries.MailSenderGmail;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.models.Volunteer;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public final class SendMailToSubscribedVolunteerHandler implements Handler<Void, List<Animal>> {

    private final VolunteerRepositoryInterface volunteerRepository;
    private final MailSenderGmail mailSenderGmail;
    
    public SendMailToSubscribedVolunteerHandler(
            VolunteerRepositoryInterface volunteerRepository,
            MailSenderGmail mailSenderGmail)
    {
        this.volunteerRepository = volunteerRepository;
        this.mailSenderGmail = mailSenderGmail;
    }


    @Override
    public Void handle(List<Animal> animals) {
        String mail = mailSenderGmail.generateMailBody(animals);
        this.volunteerRepository.getAll().stream().filter(Volunteer::isSubscribedToNewsletters).forEach(volunteer -> {
            try {
                this.mailSenderGmail.sendMail(mail, volunteer.getMail());
            } catch (UnsupportedEncodingException | MessagingException e) {
                throw new HandlerException(ErrorCode.ERROR_SEND_MAIL, volunteer.getMail());
            }
        });
        return null;
    }






}
