package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.application.dtos.mission.NewMissionDto;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.Volunteer;

import java.util.List;
import java.util.stream.Collectors;

public final class GetAvailableVolunteersHandler implements Handler<List<Volunteer>, NewMissionDto>
{
    private final VolunteerRepositoryInterface volunteerRepository;

    public GetAvailableVolunteersHandler(VolunteerRepositoryInterface volunteerRepository)
    {
        this.volunteerRepository = volunteerRepository;
    }

    @Override
    public List<Volunteer> handle(NewMissionDto dto)
    {
        return this.volunteerRepository.getAll().stream()
                .filter(volunteer -> volunteer.isAvailableFor(dto.period))
                .collect(Collectors.toList());
    }
}
