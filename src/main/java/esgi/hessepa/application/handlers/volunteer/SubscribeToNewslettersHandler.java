package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.application.dtos.volunteer.SubscribeToNewLettersDto;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.application.handlers.exception.ErrorCode;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.*;
import esgi.hessepa.domain.valueObjects.EntityId;

public final class SubscribeToNewslettersHandler implements Handler<Volunteer, SubscribeToNewLettersDto> {
    private final VolunteerRepositoryInterface volunteerRepository;

    public SubscribeToNewslettersHandler(VolunteerRepositoryInterface volunteerRepository)
    {
        this.volunteerRepository = volunteerRepository;
    }

    @Override
    public Volunteer handle(SubscribeToNewLettersDto subscriberDto)
    {
        Volunteer volunteer = this.volunteerRepository.getById(EntityId.fromString(subscriberDto.id));
        if (volunteer == null) {
            throw new HandlerException(ErrorCode.NO_MATCHING, subscriberDto.id);
        }

        volunteer = Volunteer.of(
                EntityId.fromString(subscriberDto.id),
                volunteer.getMail(),
                volunteer.getPassword(),
                volunteer.getName(),
                volunteer.getRole(),
                volunteer.getJob(),
                volunteer.getPictureUrl(),
                volunteer.getMissions(),
                subscriberDto.wantToSubscribe);

        this.volunteerRepository.save(volunteer);

        return volunteer;
    }
}
