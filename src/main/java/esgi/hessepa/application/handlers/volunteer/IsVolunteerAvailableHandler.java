package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.application.dtos.volunteer.IsVolunteerAvailableDto;
import esgi.hessepa.domain.valueObjects.WindowedPeriod;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;

import java.util.stream.Collectors;

public class IsVolunteerAvailableHandler implements Handler<Boolean, IsVolunteerAvailableDto>
{
    private final VolunteerRepositoryInterface volunteerRepository;

    public IsVolunteerAvailableHandler(VolunteerRepositoryInterface volunteerRepository)
    {
        this.volunteerRepository = volunteerRepository;
    }

    public Boolean handle(IsVolunteerAvailableDto dto)
    {
        // TODO: refacto
        return this.volunteerRepository.getAll().stream()
                .filter(v -> v.getMail().equals(dto.volunteerMail))
                .sorted()
                .collect(Collectors.toList())
                .get(0)
                .isAvailableFor(new WindowedPeriod(dto.startDate, dto.endDate));
    }
}
