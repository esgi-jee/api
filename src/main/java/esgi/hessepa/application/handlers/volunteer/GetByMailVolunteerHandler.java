package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.application.handlers.exception.ErrorCode;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.Volunteer;

public final class GetByMailVolunteerHandler implements Handler<Volunteer, String>
{
    private final VolunteerRepositoryInterface volunteerRepository;

    public GetByMailVolunteerHandler(VolunteerRepositoryInterface volunteerRepository)
    {
        this.volunteerRepository = volunteerRepository;
    }

    @Override
    public Volunteer handle(String mail)
    {
        return this.volunteerRepository.getAll().stream()
            .filter(volunteer -> volunteer.getMail().equals(mail))
            .findFirst()
            .orElseThrow(() -> {
                throw new HandlerException(ErrorCode.NO_MATCHING, mail);
            });
    }
}
