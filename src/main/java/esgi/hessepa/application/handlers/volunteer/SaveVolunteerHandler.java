package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.application.dtos.volunteer.SaveVolunteerDto;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.application.handlers.exception.ErrorCode;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.Job;
import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.models.exceptions.UnknownJobException;
import esgi.hessepa.infrastructure.web.controllers.configurations.VolunteerConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.HashSet;

public final class SaveVolunteerHandler implements Handler<String, SaveVolunteerDto>
{
    private final VolunteerRepositoryInterface volunteerRepository;
    private final VolunteerConfiguration volunteerConfiguration;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public SaveVolunteerHandler(
            VolunteerRepositoryInterface volunteerRepository,
            VolunteerConfiguration volunteerConfiguration,
            BCryptPasswordEncoder bCryptPasswordEncoder)
    {
        this.volunteerRepository = volunteerRepository;
        this.volunteerConfiguration = volunteerConfiguration;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public String handle(SaveVolunteerDto dto)
    {
        if (!this.volunteerConfiguration.isMailValid(dto.mail))
        {
            throw new HandlerException(ErrorCode.INVALID_MAIL, dto.mail);
        }

        if (!this.volunteerConfiguration.isPasswordValid(dto.password))
        {
            throw new HandlerException(ErrorCode.PASSWORD, dto.password);
        }

        this.volunteerRepository.getAll().stream()
                .filter(volunteer -> volunteer.getMail().equals(dto.mail)).findAny()
                .ifPresent(error -> {
                    throw new HandlerException(ErrorCode.ALREADY_REGISTERED_MAIL, dto.mail);
                });

        try {
            Volunteer volunteer = Volunteer.of(
                    dto.mail,
                    this.encryptPassword(dto.password),
                    dto.name,
                    Job.of(dto.job).value,
                    dto.pictureUrl,
                    new HashSet<>(),
                    dto.isSubscribedToNewsletters
            );

            this.volunteerRepository.save(volunteer);

            return volunteer.getMail();

        } catch (UnknownJobException e) {
            throw new HandlerException(ErrorCode.UNKNOWN_JOB, dto.job);
        }
    }

    private String encryptPassword(String password)
    {
        return this.bCryptPasswordEncoder.encode(password);
    }
}
