package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;

public final class DeleteVolunteerHandler implements Handler<Void, Volunteer>
{
    private final VolunteerRepositoryInterface volunteerRepository;

    public DeleteVolunteerHandler(VolunteerRepositoryInterface volunteerRepository)
    {
        this.volunteerRepository = volunteerRepository;
    }

    @Override
    public Void handle(Volunteer volunteer)
    {
        this.volunteerRepository.delete(volunteer);
        return null;
    }
}
