package esgi.hessepa.application.handlers.volunteer;

import esgi.hessepa.kernel.handlers.Handler;
import esgi.hessepa.application.handlers.exception.ErrorCode;
import esgi.hessepa.application.handlers.exception.HandlerException;
import esgi.hessepa.kernel.repositories.VolunteerRepositoryInterface;
import esgi.hessepa.domain.models.Volunteer;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.ArrayList;
import java.util.List;

public final class GetByNameVolunteersHandler implements Handler<List<Volunteer>, String>
{
    private final double LEVENSHTEIN_SCORE_THRESHOLD = 3.5;

    private final VolunteerRepositoryInterface volunteerRepository;
    private final LevenshteinDistance LevenshteinDistance;

    public GetByNameVolunteersHandler(
            VolunteerRepositoryInterface volunteerRepository,
            LevenshteinDistance LevenshteinDistance)
    {
        this.volunteerRepository = volunteerRepository;
        this.LevenshteinDistance = LevenshteinDistance;
    }

    @Override
    public List<Volunteer> handle(String name)
    {
        List<Volunteer> volunteers = new ArrayList<>();

        this.volunteerRepository.getAll().forEach(volunteer ->
        {
            if (this.LevenshteinDistance.apply(name, volunteer.getName()) <= LEVENSHTEIN_SCORE_THRESHOLD)
            {
                volunteers.add(volunteer);
            }
        });

        if (volunteers.isEmpty())
        {
            throw new HandlerException(ErrorCode.NO_MATCHING, "for name " + name);
        }

        return volunteers;
    }
}
