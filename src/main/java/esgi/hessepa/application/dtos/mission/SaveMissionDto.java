package esgi.hessepa.application.dtos.mission;

import esgi.hessepa.application.dtos.HandlerDto;
import esgi.hessepa.domain.valueObjects.EntityId;

import java.time.LocalDateTime;
import java.util.List;

public final class SaveMissionDto extends HandlerDto
{
    public final String description;
    public final List<EntityId> animals;
    public final List<String> volunteers;
    public final LocalDateTime startDate;
    public final LocalDateTime endDate;

    public SaveMissionDto(
            String description,
            List<EntityId> animals,
            List<String> volunteers,
            LocalDateTime startDate,
            LocalDateTime endDate)
    {
        this.description = description;
        this.animals = animals;
        this.volunteers = volunteers;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
