package esgi.hessepa.application.dtos.mission;

import esgi.hessepa.domain.valueObjects.WindowedPeriod;

public final class NewMissionDto {
    public final WindowedPeriod period;

    public NewMissionDto(WindowedPeriod period) {
        this.period = period;
    }
}
