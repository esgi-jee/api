package esgi.hessepa.application.dtos.volunteer;

import esgi.hessepa.application.dtos.HandlerDto;

public final class SaveVolunteerDto extends HandlerDto
{
    public final String name;
    public final String mail;
    public final String password;
    public final String job;
    public final String pictureUrl;
    public boolean isSubscribedToNewsletters;

    public SaveVolunteerDto(
            String name,
            String mail,
            String password,
            String job,
            String pictureUrl,
            boolean isSubscribedToNewsletters)
    {
        this.name = name;
        this.mail = mail;
        this.password = password;
        this.job = job;
        this.pictureUrl = pictureUrl;
        this.isSubscribedToNewsletters = isSubscribedToNewsletters;
    }
}
