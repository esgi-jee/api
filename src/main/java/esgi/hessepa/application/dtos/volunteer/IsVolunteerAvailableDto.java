package esgi.hessepa.application.dtos.volunteer;

import esgi.hessepa.application.dtos.HandlerDto;
import esgi.hessepa.domain.valueObjects.EntityId;

import java.time.LocalDateTime;

public final class IsVolunteerAvailableDto extends HandlerDto
{
    public final String volunteerMail;
    public final LocalDateTime startDate;
    public final LocalDateTime endDate;

    public IsVolunteerAvailableDto(
            String volunteerMail,
            LocalDateTime startDate,
            LocalDateTime endDate)
    {
        this.volunteerMail = volunteerMail;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
