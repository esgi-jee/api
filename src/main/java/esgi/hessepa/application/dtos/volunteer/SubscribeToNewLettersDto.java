package esgi.hessepa.application.dtos.volunteer;

import esgi.hessepa.application.dtos.HandlerDto;

public final class SubscribeToNewLettersDto extends HandlerDto {
    public final String id;
    // TODO: A rename
    public final boolean wantToSubscribe;

    public SubscribeToNewLettersDto(String id, boolean wantToSubscribe) {
        this.id = id;
        this.wantToSubscribe = wantToSubscribe;
    }
}
