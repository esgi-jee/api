package esgi.hessepa.application.dtos.animal;

import esgi.hessepa.application.dtos.HandlerDto;

public final class SaveAnimalDto extends HandlerDto
{
    public final String name;
    public final String species;
    public final String description;
    public final String pictureUrl;

    public SaveAnimalDto(
            String name,
            String species,
            String description,
            String pictureUrl)
    {
        this.name = name;
        this.species = species;
        this.description = description;
        this.pictureUrl = pictureUrl;
    }
}
