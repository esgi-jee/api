package esgi.hessepa.kernel.jpaRepositories;

import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.entities.VolunteerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaVolunteerRepo extends JpaRepository<VolunteerEntity, String>
{

}
