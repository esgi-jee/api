package esgi.hessepa.kernel.jpaRepositories;

import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.entities.AnimalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaAnimalRepo extends JpaRepository<AnimalEntity, String>
{

}
