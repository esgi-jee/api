package esgi.hessepa.kernel.jpaRepositories;

import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.entities.MissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaMissionRepo extends JpaRepository<MissionEntity, String>
{

}
