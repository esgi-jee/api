package esgi.hessepa.kernel.handlers;

public interface Handler<Return, Parameter>
{
    Return handle(Parameter dto);
}
