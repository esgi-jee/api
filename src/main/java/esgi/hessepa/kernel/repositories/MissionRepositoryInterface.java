package esgi.hessepa.kernel.repositories;

import esgi.hessepa.domain.models.Mission;
import esgi.hessepa.domain.valueObjects.EntityId;

import java.util.List;

public interface MissionRepositoryInterface
{
    void save(Mission entity);
    List<Mission> getAll();
    Mission getById(EntityId id);
    void removeAll();
    void removeById(EntityId id);
}
