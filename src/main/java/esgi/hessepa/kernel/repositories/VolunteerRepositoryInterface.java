package esgi.hessepa.kernel.repositories;

import esgi.hessepa.domain.models.Volunteer;
import esgi.hessepa.domain.valueObjects.EntityId;
import esgi.hessepa.infrastructure.jpa.entities.VolunteerEntity;

import java.util.List;

public interface VolunteerRepositoryInterface
{
    void save(Volunteer entity);
    List<Volunteer> getAll();
    Volunteer getById(EntityId id);
    void removeAll();
    void removeById(EntityId id);

    void delete(Volunteer dto);
}
