package esgi.hessepa.kernel.repositories;

import esgi.hessepa.domain.models.Animal;
import esgi.hessepa.domain.valueObjects.EntityId;

import java.util.List;

public interface AnimalRepositoryInterface
{
    void save(Animal entity);
    List<Animal> getAll();
    Animal getById(EntityId id);
    void removeAll();
    void removeById(EntityId id);
}
