FROM openjdk:11.0.6-jre-slim

USER root

COPY  target/*.jar api.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=$profiles", "api.jar"]